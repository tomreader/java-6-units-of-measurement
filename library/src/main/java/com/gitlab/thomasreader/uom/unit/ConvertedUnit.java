/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;

import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * Represents a unit with a {@link UnitConversion} used to convert to and from a reference unit.
 *
 * @param <T> the quantity type
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Unit_of_measurement">Wikipedia - Unit of measurement</a>
 * @since 0.1.0
 */
public class ConvertedUnit<T extends Quantity<T>> extends AbstractUnit<T> {

    @Nonnull
    private final Unit<T> reference;
    @Nonnull
    private final UnitConversion conversion;
    private final String symbol;

    /**
     * Instantiates a new Converted unit.
     *
     * @param symbol     the symbol
     * @param reference   the reference unit
     * @param conversion the conversion to the reference unit
     */
    public ConvertedUnit(String symbol, @Nonnull Unit<T> reference, @Nonnull UnitConversion conversion) {
        this(symbol, reference, conversion, true, null);
    }

    /**
     * Instantiates a new Converted unit.
     *
     * @param symbol       the symbol
     * @param reference     the reference unit
     * @param conversion   the conversion to the reference unit
     * @param canPrefix    whether this unit can be prefixed
     * @param symbolFormat the symbol format
     */
    public ConvertedUnit(String symbol,
                         @Nonnull Unit<T> reference,
                         @Nonnull UnitConversion conversion,
                         boolean canPrefix,
                         UnitSymbolFormat symbolFormat) {
        super(canPrefix, symbolFormat);
        this.symbol = symbol;
        this.reference = reference;
        this.conversion = conversion;
    }

    @Override
    public String getSymbol() {
        return this.symbol;
    }

    @Nonnull
    @Override
    public Class<T> getType() {
        return this.getReferenceUnit().getType();
    }

    @Nonnull
    @Override
    public UnitConversion getConversion() {
        return this.conversion;
    }

    @Nonnull
    @Override
    public Unit<T> getReferenceUnit() {
        return this.reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConvertedUnit<?> that = (ConvertedUnit<?>) o;
        return getReferenceUnit().equals(that.getReferenceUnit()) &&
                getConversion().equals(that.getConversion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReferenceUnit(), getConversion());
    }
}
