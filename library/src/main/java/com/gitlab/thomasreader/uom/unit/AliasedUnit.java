/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;

import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * A unit which is an alias or synonym for another unit. For example litre is an alias for
 * cubic decimetre.
 *
 * @param <T> the quantity type
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Litre">Wikipedia - Litre</a>
 * @since 0.1.0
 */
public class AliasedUnit<T extends Quantity<T>> extends AbstractUnit<T> {
    @Nonnull
    private final Unit<T> compositeUnit;
    @Nonnull
    private final String symbol;

    /**
     * Instantiates a new Aliased unit.
     *
     * @param symbol        the symbol
     * @param compositeUnit the composite unit
     */
    public AliasedUnit(@Nonnull String symbol, @Nonnull Unit<T> compositeUnit) {
        this(symbol, compositeUnit, true, null);
    }

    /**
     * Instantiates a new Aliased unit.
     *
     * @param symbol        the symbol
     * @param compositeUnit the composite unit
     * @param canPrefix  whether this can be prefixed
     * @param symbolFormat the symbol formatter
     */
    public AliasedUnit(@Nonnull String symbol,
                       @Nonnull Unit<T> compositeUnit,
                       boolean canPrefix,
                       UnitSymbolFormat symbolFormat) {
        super(canPrefix, symbolFormat);
        this.compositeUnit = compositeUnit;
        this.symbol = symbol;
    }

    @Override
    public String getSymbol() {
        return this.symbol;
    }

    @Nonnull
    @Override
    public Unit<T> getReferenceUnit() {
        return this.getCompositeUnit().getReferenceUnit();
    }

    /**
     * Gets the composite unit.
     *
     * @return the composite unit
     */
    @Nonnull
    public Unit<T> getCompositeUnit() {
        return this.compositeUnit;
    }

    @Nonnull
    @Override
    public Class<T> getType() {
        return this.getCompositeUnit().getType();
    }

    @Nonnull
    @Override
    public UnitConversion getConversion() {
        return this.getCompositeUnit().getConversion();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AliasedUnit<?> that = (AliasedUnit<?>) o;
        return getCompositeUnit().equals(that.getCompositeUnit()) &&
                getSymbol().equals(that.getSymbol());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCompositeUnit(), getSymbol());
    }
}
