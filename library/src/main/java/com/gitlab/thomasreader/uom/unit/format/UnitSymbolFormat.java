/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit.format;

import com.gitlab.thomasreader.uom.unit.Unit;

import javax.annotation.Nonnull;

/**
 * Functional interface to format the unit symbol.
 * Most units format the symbol with a separator after the value of the quantity.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://www.bipm.org/en/publications/si-brochure/">
 *     BIPM - SI Brochure, Section: 5.4.3</a>
 * @since   0.1.0
 */
public interface UnitSymbolFormat {

    /**
     * The default unit symbol format using a space separator.
     */
    @Nonnull
    public static final UnitSymbolFormat DEFAULT = new UnitSymbolFormat() {
        @Nonnull
        @Override
        public String format(@Nonnull Unit<?> unit) {
            return (unit.getSymbol() != null)? " " + unit.getSymbol() : "";
        }
    };

    /**
     * The unit symbol format where no spacing is used for units such as degrees or shorthand
     * feet and inches (' and ").
     */
    @Nonnull
    public static final UnitSymbolFormat NO_SEPARATOR = new UnitSymbolFormat() {
        @Nonnull
        @Override
        public String format(@Nonnull Unit<?> unit) {
            return (unit.getSymbol() != null)? unit.getSymbol() : "";
        }
    };

    /**
     * Return the formatted symbol for the given unit.
     *
     * @param unit the unit
     * @return the formatted symbol
     */
    @Nonnull
    public String format(@Nonnull Unit<?> unit);
}
