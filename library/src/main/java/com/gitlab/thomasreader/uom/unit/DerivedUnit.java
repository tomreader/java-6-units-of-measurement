/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.LinearConversion;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;
import com.gitlab.thomasreader.uom.util.SuperscriptFormat;
import com.gitlab.thomasreader.uom.math.BigDecRational;
import com.gitlab.thomasreader.uom.quantity.type.AmountOfSubstance;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.quantity.type.ElectricCurrent;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.quantity.type.LuminousIntensity;
import com.gitlab.thomasreader.uom.quantity.type.Mass;
import com.gitlab.thomasreader.uom.quantity.type.Temperature;
import com.gitlab.thomasreader.uom.quantity.type.Time;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * Helper class to conveniently create derived units from other units.
 *
 * @param <T> the quantity type
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/SI_derived_unit">Wikipedia - SI derived unit</a>
 * @since 0.1.0
 */
public class DerivedUnit<T extends Quantity<T>> extends AbstractUnit<T> {

    @Nonnull
    private final String symbol;
    @Nonnull
    private final Map<Unit<?>, Integer> unitsExponents;
    @Nonnull
    private final MultiplicativeConversion conversion;
    @Nonnull
    private final Unit<T> reference;

    /**
     * Instantiates a new Derived unit.
     *
     * @param symbol         the symbol
     * @param unitsExponents the map of units to make this derived unit
     * @param conversion     the conversion to the reference
     * @param reference      the reference unit
     * @param symbolFormat   the symbol format
     */
    DerivedUnit(@Nonnull String symbol,
                       @Nonnull Map<Unit<?>, Integer> unitsExponents,
                       @Nonnull MultiplicativeConversion conversion,
                       @Nonnull Unit<T> reference,
                       UnitSymbolFormat symbolFormat) {
        super(false, symbolFormat);
        this.symbol = symbol;
        this.unitsExponents = unitsExponents;
        this.conversion = conversion;
        this.reference = reference;
    }

    @Nonnull
    @Override
    public String getSymbol() {
        return this.symbol;
    }

    @Nonnull
    @Override
    public Unit<T> getReferenceUnit() {
        return this.reference;
    }

    @Nonnull
    @Override
    public Class<T> getType() {
        return this.getReferenceUnit().getType();
    }

    @Nonnull
    @Override
    public MultiplicativeConversion getConversion() {
        return this.conversion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DerivedUnit<?> that = (DerivedUnit<?>) o;
        return unitsExponents.equals(that.unitsExponents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unitsExponents);
    }

    /**
     * Returns the builder to create derived units.
     *
     * @return the builder
     */
    @Nonnull
    public static Builder builder() {
        return new DerivedUnit.Builder();
    }

    /**
     * Builder to create derived units.
     *
     * @author Tom Reader
     * @version 0.1.0
     * @since 0.1.0
     */
    public static class Builder {
        @Nonnull
        private final Map<Unit<?>, Integer> units;
        private String symbol;
        private UnitSymbolFormat symbolFormat;

        /**
         * Instantiates a new Builder.
         */
        Builder() {
            this.units = new HashMap<Unit<?>, Integer>();
        }

        /**
         * Sets a defined symbol.
         *
         * @param symbol the symbol
         * @return the builder
         */
        public Builder setSymbol(String symbol) {
            this.symbol = symbol;
            return this;
        }

        /**
         * Sets the symbol format.
         *
         * @param symbolFormat the symbol format
         * @return the builder
         */
        public Builder setSymbolFormat(UnitSymbolFormat symbolFormat) {
            this.symbolFormat = symbolFormat;
            return this;
        }

        /**
         * Add a unit as a quotient.
         *
         * @param unit the quotient unit
         * @return the builder
         */
        @Nonnull
        public Builder divide(Unit<?> unit) {
            return this.multiply(unit, -1);
        }

        /**
         * Add a unit as a multiplier.
         *
         * @param unit the quotient unit
         * @return the builder
         */
        @Nonnull
        public Builder multiply(Unit<?> unit) {
            return this.multiply(unit, 1);
        }

        /**
         * Add a unit to the provided power. Negative powers model division of the unit.
         *
         * @param unit the unit
         * @param pow  the power to raise the unit by
         * @return the builder
         */
        @Nonnull
        public Builder multiply(Unit<?> unit, int pow) {
            if (unit.getType() == Dimensionless.class &&
                    unit instanceof ReferenceUnit) {
                // dimensionless reference unit aka "ONE"
                return this;
            }
            if (unit instanceof DerivedUnit) {
                Map<Unit<?>, Integer> derivedUnitMap = ((DerivedUnit<?>) unit).unitsExponents;
                for (Unit<?> key : derivedUnitMap.keySet()) {
                    this.multiply(key, derivedUnitMap.get(key) * pow);
                }
            } else if (unit.getConversion() instanceof LinearConversion) {
                if (this.units.containsKey(unit)) {
                    int newCount = this.units.get(unit) + pow;
                    if (newCount == 0) {
                        this.units.remove(unit);
                    } else {
                        this.units.put(unit, newCount);
                    }
                } else if (pow != 0) {
                    this.units.put(unit, pow);
                }
            }
            return this;
        }

        /**
         * Builds the derived unit using {@link java.math.BigDecimal}.
         *
         * @param <T>       the quantity type
         * @param reference the reference unit
         * @return the derived unit
         * @throws IllegalStateException if one or less units were added
         */
        @Nonnull
        public <T extends Quantity<T>> DerivedUnit<T> buildDecimal(Unit<T> reference) throws IllegalStateException {
            return build(reference,true);
        }

        /**
         * Builds the derived unit using doubles.
         *
         * @param <T>       the quantity type
         * @param reference the reference unit
         * @return the derived unit
         * @throws IllegalStateException if one or less units were added
         */
        @Nonnull
        public <T extends Quantity<T>> DerivedUnit<T> buildDouble(Unit<T> reference) throws IllegalStateException {
            return build(reference,false);
        }

        private static int getUnitTypePosition(Unit<?> unit) {
            Class<?> type = unit.getType();
            if (type.equals(Dimensionless.class)) {
                return -2;
            } else if (type.equals(Length.class)) {
                return 0;
            } else if (type.equals(Mass.class)) {
                return 1;
            } else if (type.equals(Time.class)) {
                return 2;
            } else if (type.equals(ElectricCurrent.class)) {
                return 3;
            } else if (type.equals(Temperature.class)) {
                return 4;
            } else if (type.equals(AmountOfSubstance.class)) {
                return 5;
            } else if (type.equals(LuminousIntensity.class)) {
                return 6;
            } else {
                return -1;
            }
        }

        @Nonnull
        private <T extends Quantity<T>> DerivedUnit<T> build(Unit<T> reference, boolean decimal) throws IllegalStateException {
            if (this.units.isEmpty()) {
                throw new IllegalStateException("DerivedUnit.Builder: No units added");
            }
            if (this.units.size() == 1) {
                int pow = this.units.values().iterator().next();
                if (pow >= -1 && pow <= 1) {
                    throw new IllegalStateException(
                            "DerivedUnit.Builder: contains only one unit raised to a power between -1 and 1"
                    );
                }
            }

            double result = 1;
            BigDecRational decimalResult = BigDecRational.ONE;
            List<Unit<?>> sortedUnits = new ArrayList<Unit<?>>(this.units.keySet());
            Collections.sort(sortedUnits, new Comparator<Unit<?>>() {
                @Override
                public int compare(Unit<?> unit1, Unit<?> unit2) {
                    return Integer.compare(getUnitTypePosition(unit1), getUnitTypePosition(unit2));
                }
            });

            boolean generateSymbol = (this.symbol == null || this.symbol == "");
            StringBuilder symbolBuilder = null;
            if (generateSymbol) {
                symbolBuilder = new StringBuilder((sortedUnits.size() * 2));
            }
            for (int i = 0; i < sortedUnits.size(); i++) {
                Unit<?> unit = sortedUnits.get(i);
                if (this.units.containsKey(unit)) {
                    int pow = this.units.get(unit);
                    if (pow != 0) {
                        if (decimal) {
                            decimalResult = decimalResult.multiply(
                                    ((LinearConversion) unit.getConversion()).getRationalCoefficient().pow(pow)
                            );
                        } else {
                            result *= Math.pow(((LinearConversion) unit.getConversion()).getCoefficient(), pow);
                        }

                        if (generateSymbol) {
                            symbolBuilder.append(unit.getSymbol());
                            if (pow != 1) {
                                symbolBuilder.append(SuperscriptFormat.fromInt(pow));
                            }
                            if (i + 1 != sortedUnits.size()) {
                                symbolBuilder.append('⋅');
                            }
                        }
                    }
                }
            }

            if (generateSymbol) {
                this.symbol = symbolBuilder.toString();
            }

            MultiplicativeConversion multiplicativeConversion =
                    (decimal)? Conversions.multiply(decimalResult) : Conversions.multiply(result);

            return new DerivedUnit<T>(this.symbol, units, multiplicativeConversion, reference, this.symbolFormat);
        }
    }
}
