/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;

import javax.annotation.Nonnull;

/**
 * Models a conversion which is a composition of other conversions. Useful for
 * factors such as the one needed for converting
 * <a href="https://en.wikipedia.org/wiki/Fahrenheit">Fahrenheit</a> to Celsius.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class CompositeConversion implements UnitConversion {
    @Nonnull
    private final UnitConversion[] conversions;

    /**
     * Instantiates a new Composite conversion.
     *
     * @param conversions
     *         the ordered conversions
     * @throws IllegalArgumentException if {@code conversions} length is zero
     */
    public CompositeConversion(@Nonnull UnitConversion... conversions) throws IllegalArgumentException {
        if (conversions.length == 0) {
            throw new IllegalArgumentException("CompositeConversion: no conversions supplied");
        }
        this.conversions = conversions;
    }


    /**
     * Applies each {@link LinearConversion} in the order in which they were added
     * to {@code this} using double precision - faster than
     * {@link #convert(BigDecimal, MathContext)} but less precise.
     *
     * @param input the input to be applied by this
     * @return result after applying each {@link LinearConversion} part of {@code this}
     * @see LinearConversion#convert(double)
     */
    @Override
    public double convert(double input) {
        int limit = conversions.length;
        double result = input;
        for (int i = 0; i < limit; i++) {
            result = conversions[i].convert(result);
        }
        return result;
    }

    /**
     * Applies each {@link LinearConversion} in reverse order in which they were added
     * to {@code this} using double precision - faster than
     * {@link #convert(BigDecimal, MathContext)} but less precise.
     *
     * @param input the input to be applied by this
     * @return result after applying each {@link LinearConversion} part of {@code this}
     * @see LinearConversion#inverse(double)
     */
    @Override
    public double inverse(double input) {
        int start = conversions.length - 1;
        double result = input;
        for (int i = start; i >= 0; i--) {
            result = conversions[i].inverse(result);
        }
        return result;
    }

    /**
     * Applies each {@link LinearConversion} in the order in which they were added
     * to {@code this} using BigDecimal precision.
     *
     * @param input the input to be applied by {@code this}
     * @param mathContext the MathContext to use for operations
     * @return result after applying each {@link LinearConversion} part of {@code this}
     * @see LinearConversion#convert(BigDecimal, MathContext)
     */
    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        int limit = conversions.length;
        BigDecimal result = input;
        for (int i = 0; i < limit; i++) {
            result = conversions[i].convert(result, mathContext);
        }
        return result;
    }

    /**
     * Applies each {@link LinearConversion} in reverse order in which they were added
     * to {@code this} using BigDecimal precision.
     *
     * @param input the input to be applied by {@code this}
     * @param mathContext the MathContext to use for operations
     * @return result after applying each {@link LinearConversion} part of {@code this}
     * @see LinearConversion#inverse(BigDecimal, MathContext)
     */
    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        int start = conversions.length - 1;
        BigDecimal result = input;
        for (int i = start; i >= 0; i--) {
            result = conversions[i].inverse(result, mathContext);
        }
        return result;
    }

    @Nonnull
    @Override
    public String toString() {
        int length = this.conversions.length;
        StringBuilder stringBuilder = new StringBuilder(length * 7);
        stringBuilder.append("(");
        for (int i = 0; i < length; i++) {
            stringBuilder.append(this.conversions[i].toString());
        }
        return stringBuilder.append(")")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeConversion that = (CompositeConversion) o;
        return Arrays.equals(conversions, that.conversions);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(conversions);
    }
}
