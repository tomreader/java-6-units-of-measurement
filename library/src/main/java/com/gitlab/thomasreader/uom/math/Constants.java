/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.math;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * Useful mathematical constants.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class Constants {

    /**
     * Pi in the form of a big decimal with 34 digits of precision.
     *
     * @param mathContext the math context to round pi by
     * @return pi to the precision specified by the mathContext
     */
    @Nonnull
    public static BigDecimal pi(MathContext mathContext) {
        if (mathContext == null) mathContext = MathContext.DECIMAL128;
        return new BigDecimal("3.141592653589793238462643383279503", mathContext);
    }

    /**
     * Euler's number in the form of a big decimal with 34 digits of precision.
     *
     * @param mathContext the math context to round e by
     * @return euler's number to the precision specified by the mathContext
     */
    @Nonnull
    public static BigDecimal e(MathContext mathContext) {
        if (mathContext == null) mathContext = MathContext.DECIMAL128;
        return new BigDecimal("2.718281828459045235360287471352662", mathContext);
    }
}
