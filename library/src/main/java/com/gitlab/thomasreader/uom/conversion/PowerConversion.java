/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * Specialisation of a {@link MultiplicativeConversion} which models a function of the form
 * {@code base^exponent}.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class PowerConversion implements MultiplicativeConversion {
    private final int base;
    private final int exponent;

    /**
     * Instantiates a new power conversion.
     *
     * @param base     the base
     * @param exponent the exponent
     */
    public PowerConversion(int base, int exponent) {
        this.base = base;
        this.exponent = exponent;
    }

    @Override
    public double getCoefficient() {
        return Math.pow(this.base, this.exponent);
    }

    @Nonnull
    @Override
    public BigDecRational getRationalCoefficient() {
        return BigDecRational.valueOf(base).pow(this.exponent);
    }

    /**
     * Gets decimal factor.
     *
     * @return the decimal factor
     */
    private BigDecimal getDecimalFactor() {
        return BigDecimal.valueOf(base).pow(this.exponent, MathContext.DECIMAL128);
    }

    @Override
    public double convert(double input) {
        // provide slightly better precision by utilising whole numbers
        if (exponent >= 0) {
            return input * this.getCoefficient();
        } else {
            return input / Math.pow(this.base, -this.exponent);
        }
    }

    @Override
    public double inverse(double input) {
        // provide slightly better precision by utilising whole numbers
        if (exponent >= 0) {
            return input / this.getCoefficient();
        } else {
            return input * Math.pow(this.base, -this.exponent);
        }
    }

    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return input.multiply(this.getDecimalFactor(), mathContext);
    }

    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return input.divide(this.getDecimalFactor(), mathContext);
    }
}
