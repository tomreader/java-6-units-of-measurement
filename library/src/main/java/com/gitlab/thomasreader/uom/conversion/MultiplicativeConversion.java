/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * A conversion which resembles a
 * <a href="https://en.wikipedia.org/wiki/Multiplicative_function">multiplicative function </a>.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Multiplicative_function">Wikipedia - Multiplicative function </a>
 * @since 0.1.0
 */
public interface MultiplicativeConversion extends LinearConversion {
    // https://en.wikipedia.org/wiki/Multiplicative_function

    public static final IdentityConversion IDENTITY = new IdentityConversion();

    static class IdentityConversion implements MultiplicativeConversion {

        // https://en.wikipedia.org/wiki/Identity_element#Definitions

        private IdentityConversion() {
        }

        @Override
        public double getCoefficient() {
            return 1;
        }

        @Nonnull
        @Override
        public BigDecRational getRationalCoefficient() {
            return BigDecRational.ONE;
        }

        @Override
        public double convert(double input) {
            return input;
        }

        @Override
        public double inverse(double input) {
            return input;
        }

        @Nonnull
        @Override
        public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
            return input;
        }

        @Nonnull
        @Override
        public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
            return input;
        }
    }
}
