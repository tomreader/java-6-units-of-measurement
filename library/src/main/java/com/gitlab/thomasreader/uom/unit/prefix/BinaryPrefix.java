/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit.prefix;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.PrefixedUnit;
import com.gitlab.thomasreader.uom.unit.Unit;

import javax.annotation.Nonnull;

/**
 * <p>
 * Models a <a href="https://en.wikipedia.org/wiki/Binary_prefix">binary prefix</a> for multiples
 * of {@link Unit units} in data processing, data transmission, and digital information, notably
 * the bit and the byte, to indicate multiplication by a power of 2.
 * </p>
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Binary_prefix">Wikipedia - Binary prefix</a>
 * @since 0.1.0
 */
public enum BinaryPrefix implements UnitPrefix {
    /**
     * 2<sup>80</sup>
     */
    YOBI("Yi", 80),
    /**
     * 2<sup>70</sup>
     */
    ZEBI("Zi", 70),
    /**
     * 2<sup>60</sup>
     */
    EXBI("Ei", 60),
    /**
     * 2<sup>50</sup>
     */
    PEBI("Pi", 50),
    /**
     * 2<sup>40</sup>
     */
    TEBI("Ti", 40),
    /**
     * 2<sup>30</sup>
     */
    GIBI("Gi", 30),
    /**
     * 2<sup>20</sup>
     */
    MEBI("Mi", 20),
    /**
     * 2<sup>10</sup>
     */
    KIBI("Ki", 10);

    @Nonnull
    private final String symbol;

    private final int exponent;

    BinaryPrefix(@Nonnull String symbol, int exponent) {
        this.symbol = symbol;
        this.exponent = exponent;
    }



//    @Nonnull
//    @Override
//    public UnitPrefix fold(UnitPrefix other) {
//        int combinedExponent = this.exponent + ((BinaryPrefix) prefixedUnit.getUnitPrefix()).exponent;
//        BinaryPrefix biggestPrefix = this;
//        BinaryPrefix[] binaryPrefixes = BinaryPrefix.values();
//        for (int i = 0, size = binaryPrefixes.length; i < size; i++) {
//            BinaryPrefix currentPrefix = binaryPrefixes[i];
//            if (currentPrefix.exponent == combinedExponent) {
//                return new PrefixedUnit<T>(currentPrefix, prefixedUnit.getPrefixedUnit());
//            } else if (currentPrefix.exponent < combinedExponent && currentPrefix.exponent > biggestPrefix.exponent) {
//                biggestPrefix = currentPrefix;
//            }
//        }
//    }


    @Nonnull
    @Override
    public String getSymbol() {
        return this.symbol;
    }

    @Nonnull
    @Override
    public MultiplicativeConversion getConversion() {
        return Conversions.power(2, this.exponent);
    }

    @Override
    public <T extends Quantity<T>> Unit<T> prefix(@Nonnull Unit<T> unit) {
        try {
            return this.prefixOrThrow(unit);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Nonnull
    @Override
    public <T extends Quantity<T>> Unit<T> prefixOrThrow(@Nonnull Unit<T> unit) throws IllegalArgumentException {
//        if (unit instanceof DerivedUnit) {
//            throw new IllegalArgumentException("Derived units cannot be prefixed");
//        } else if (unit instanceof PrefixedUnit) {
//            throw new IllegalArgumentException("Compound prefixes are not permitted");
//        } else
        if (!unit.canPrefix()) {
            throw new IllegalArgumentException("This unit is not permitted to be prefixed");
        }
        return new PrefixedUnit<T>(this, unit);
    }


//    /**
//     * {@inheritDoc}
//     */
//    @Nonnull
//    @Override
//    public <T extends Quantity<T>> Unit<T> prefix(@Nonnull Unit<T> unit, boolean collate) {
//        if (collate) {
//            if (unit instanceof PrefixedUnit) {
//                PrefixedUnit<T> prefixedUnit = (PrefixedUnit<T>) unit;
//                if (prefixedUnit.getUnitPrefix() instanceof BinaryPrefix) {
//                    int combinedExponent = this.exponent + ((BinaryPrefix) prefixedUnit.getUnitPrefix()).exponent;
//                    BinaryPrefix biggestPrefix = this;
//                    BinaryPrefix[] binaryPrefixes = BinaryPrefix.values();
//                    for (int i = 0, size = binaryPrefixes.length; i < size; i++) {
//                        BinaryPrefix currentPrefix = binaryPrefixes[i];
//                        if (currentPrefix.exponent == combinedExponent) {
//                            return new PrefixedUnit<T>(currentPrefix, prefixedUnit.getPrefixedUnit());
//                        } else if (currentPrefix.exponent < combinedExponent && currentPrefix.exponent > biggestPrefix.exponent) {
//                            biggestPrefix = currentPrefix;
//                        }
//                    }
//                    return new PrefixedUnit<T>(biggestPrefix, prefixedUnit.getPrefixedUnit());
//                }
//            }
//        }
//        return new PrefixedUnit<T>(this, unit);
//    }


}
