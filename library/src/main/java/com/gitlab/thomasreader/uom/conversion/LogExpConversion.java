/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * Conversion which models either a logarithmic conversion or its inverse a exponential conversion.
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Logarithm">Wikipedia - Logarithm</a>
 * @since 0.1.0
 */
public class LogExpConversion implements UnitConversion {

    private final boolean logForward;
    private final double base;
    private final double baseLog;

    private LogExpConversion(double base, boolean logForward) {
        if (base <= 0 || base == 1) {
            String message = (logForward) ? "Invalid logarithm of base " + base :
                    "Invalid exponential of base " + base;
            throw new ArithmeticException(message);
        }
        this.base = base;
        this.logForward = logForward;
        this.baseLog = Math.log(base);
    }

    /**
     * Creates a logarithmic conversion of log,base(x)
     *
     * @param base the base
     * @return the logarithmic conversion
     */
    public static LogExpConversion log(double base) {
        return new LogExpConversion(base, true);
    }

    /**
     * Creates a exponential conversion of base^x
     *
     * @param base the base
     * @return the exponential conversion
     */
    public static LogExpConversion exp(double base) {
        return new LogExpConversion(base, false);
    }

    /**
     * Gets the base.
     *
     * @return the base
     */
    public double getBase() {
        return this.base;
    }

    private double convert(double input, boolean logForward, boolean invert) {
        if (logForward && !invert || !logForward && invert) {
            if (input <= 0) {
                StringBuilder message = new StringBuilder(20 + 5);
                ((base == Math.E) ? message.append("ln") : message.append("log").append(base))
                        .append("(")
                        .append(input)
                        .append(") is undefined");
                throw new UnitConversionException(message.toString());
            }
            return Math.log(input) / baseLog;
        } else {
            return Math.pow(Math.E, (baseLog * input));
        }
    }

    @Override
    public double convert(double input) {
        return convert(input, this.logForward, false);
    }

    @Override
    public double inverse(double input) {
        return convert(input, this.logForward, true);
    }

    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return BigDecimal.valueOf(
                convert(input.doubleValue(), this.logForward, false)
        );
    }

    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return BigDecimal.valueOf(
                convert(input.doubleValue(), this.logForward, true)
        );
    }
}
