/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;
import com.gitlab.thomasreader.uom.unit.Unit;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * A conversion consisting of a rational multiplicative factor.
 * {@link Unit units}.
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class BigFracMultiplyConversion extends BigMultiplicativeConversion {
    @Nonnull
    private final BigDecRational coefficient;

    public BigFracMultiplyConversion(@Nonnull BigDecRational coefficient) {
        this.coefficient = coefficient;
    }

    @Nonnull
    @Override
    public BigDecRational getRationalCoefficient() {
        return this.coefficient;
    }

    /**
     * Multiplies the rational coefficient by the given input using {@code double} precision.
     *
     * @param input the inputted number
     * @return number after the input has been multiplied by this coefficient
     */
    @Override
    public double convert(double input) {
        return input * this.getCoefficient();
    }

    /**
     * Divides the input by the rational coefficient using {@code double} precision.
     *
     * @param input the inputted number
     * @return number after the input has been divded by this coefficient
     */
    @Override
    public double inverse(double input) {
        return input / this.getCoefficient();
    }

    /**
     * Multiplies the rational coefficient by the given input using {@code BigDecimal} precision.
     *
     * @param input the inputted number
     * @param mathContext the {@code MathContext} to use for the operation
     * @return number after the input has been multiplied by this coefficient
     * @see BigDecRational#multiply(BigDecRational)
     */
    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return BigDecRational.valueOf(input)
                .multiply(this.getRationalCoefficient())
                .bigDecimalValue(mathContext);
    }

    /**
     * Divides the input by the rational coefficient using {@code BigDecimal} precision.
     *
     * @param input the inputted number
     * @param mathContext the {@code MathContext} to use for the operation
     * @return number after the input has been multiplied by this coefficient
     * @see BigDecRational#divide(BigDecRational)
     */
    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return BigDecRational.valueOf(input)
                .divide(this.getRationalCoefficient())
                .bigDecimalValue(mathContext);
    }

    @Override
    public String toString() {
        if (this.coefficient.getNumerator().compareTo(BigDecimal.ONE) == 0) {
            return "/" + this.coefficient.getDenominator();
        } else {
            return "*" + this.coefficient.toString('/');
        }
    }
}
