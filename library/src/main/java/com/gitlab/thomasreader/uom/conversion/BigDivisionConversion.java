/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;
import com.gitlab.thomasreader.uom.unit.Unit;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * A division conversion.
 * {@link Unit units}.
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class BigDivisionConversion extends BigMultiplicativeConversion {
    @Nonnull
    private final BigDecimal coefficient;

    public BigDivisionConversion(@Nonnull BigDecimal coefficient) {
        this.coefficient = coefficient;
    }

    @Nonnull
    @Override
    public BigDecRational getRationalCoefficient() {
        return BigDecRational.valueOf(BigDecimal.ONE, this.coefficient);
    }

    /**
     * Divides the conversion factor to the given input using {@code double} precision - faster
     * than {@link #convert(BigDecimal, MathContext)} but less precise.
     *
     * @param input the inputted number
     * @return number after the input has been divided by this factor
     */
    @Override
    public double convert(double input) {
        return input * this.getCoefficient();
    }

    /**
     * Multiplies the conversion factor to the given input using {@code double} precision - faster
     * than {@link #convert(BigDecimal, MathContext)} but less precise.
     *
     * @param input the inputted number
     * @return number after the input has been multiplied by this factor
     */
    @Override
    public double inverse(double input) {
        return input / this.getCoefficient();
    }

    /**
     * Divides the conversion factor to the given input using {@code BigDecimal} precision.
     *
     * @param input the inputted number
     * @param mathContext the {@code MathContext} to use for the operation
     * @return number after the input has been divided by this factor
     * @see BigDecimal#divide(BigDecimal, MathContext)
     */
    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return input.divide(this.coefficient, mathContext);
    }

    /**
     * Multiplies the conversion factor to the given input using {@code BigDecimal} precision.
     *
     * @param input the inputted number
     * @param mathContext the {@code MathContext} to use for the operation
     * @return number after the input has been multiplied by this factor
     * @see BigDecimal#multiply(BigDecimal, MathContext)
     */
    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return input.multiply(this.coefficient, mathContext);
    }

    /**
     *
     * @return {@code / factor}
     */
    @Nonnull
    @Override
    public String toString() {
        return '/' + this.coefficient.toString();
    }
}
