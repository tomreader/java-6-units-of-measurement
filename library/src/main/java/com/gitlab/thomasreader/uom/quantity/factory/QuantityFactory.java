/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity.factory;

import com.gitlab.thomasreader.uom.quantity.DecimalQuantity;
import com.gitlab.thomasreader.uom.quantity.DoubleQuantity;
import com.gitlab.thomasreader.uom.quantity.Quantity;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

/**
 * Interface to denote that implementors can create a {@link Quantity <T>} from a value.
 *
 * @param <T> the type of quantity
 * @author  Tom Reader
 * @version 0.1.0
 * @since   0.1.0
 */
public interface QuantityFactory<T extends Quantity<T>> {

    /**
     * Creates a quantity from the value.
     *
     * @param value the value
     * @return the quantity with the value
     */
    @Nonnull
    public DoubleQuantity<T> of(double value);

    @Nonnull
    public DecimalQuantity<T> of(BigDecimal value);
}
