/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

import javax.annotation.Nonnull;

/**
 * Helper class to create {@link UnitConversion}.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class Conversions {

    protected Conversions() {
    }


    /**
     * Returns an identity conversion (multiply by one)
     *
     * @return the identity conversion
     */
    @Nonnull
    public static MultiplicativeConversion identity() {
        return MultiplicativeConversion.IDENTITY;
    }

    /**
     * Returns a Factor conversion.
     *
     * @param coefficient - the coefficient
     * @return the coefficient conversion
     */
    @Nonnull
    public static MultiplicativeConversion multiply(double coefficient) {
        if (coefficient == 1) {
            return identity();
        }
        return new DoubleMultiplyConversion(coefficient);
    }


    /**
     * Helper method to create a Factor conversion from a multiplicand conversion.
     *
     * @param multiplicand - the multiplicand conversion
     * @return the factor conversion
     * @see #multiply(double) #multiply(double)
     */
    @Nonnull
    public static MultiplicativeConversion multiply(@Nonnull BigDecimal multiplicand) {
        if (multiplicand.compareTo(BigDecimal.ONE) == 0) {
            return identity();
        }
        return new BigMultiplyConversion(multiplicand);
    }

    /**
     * Helper method to create a Factor conversion from a dividend conversion.
     *
     * @param dividend the dividend conversion
     * @return the factor conversion
     * @see #multiply(double) #multiply(double)
     */
    @Nonnull
    public static MultiplicativeConversion divide(@Nonnull BigDecimal dividend) {
        if (dividend.compareTo(BigDecimal.ONE) == 0) {
            return identity();
        }
        return new BigDivisionConversion(dividend);
    }

    /**
     * Multiply multiplicative conversion.
     *
     * @param bigRational the big rational
     * @return the multiplicative conversion
     */
    @Nonnull
    public static MultiplicativeConversion multiply(@Nonnull BigDecRational bigRational) {
        if (bigRational.compareTo(BigDecRational.ONE) == 0) {
            return identity();
        }
        return new BigFracMultiplyConversion(bigRational);
    }

    /**
     * Multiply multiplicative conversion.
     *
     * @param numerator   the numerator
     * @param denominator the denominator
     * @return the multiplicative conversion
     */
    @Nonnull
    public static MultiplicativeConversion multiply(@Nonnull BigInteger numerator, @Nonnull BigInteger denominator) {
        BigDecRational bigRational = BigDecRational.valueOf(numerator, denominator);
        if (bigRational.compareTo(BigDecRational.ONE) == 0) {
            return identity();
        }
        return new BigFracMultiplyConversion(bigRational);
    }

    /**
     * Generates a {@link MultiplicativeConversion} which models a fraction consisting of two non-decimal
     * numbers.
     *
     * @param numerator   the numerator
     * @param denominator the denominator
     * @return numerator / denominator
     */
    @Nonnull
    public static MultiplicativeConversion multiply(long numerator, long denominator) {
        if (numerator == denominator) {
            return identity();
        }
        return new BigFracMultiplyConversion(
                BigDecRational.valueOf(numerator, denominator)
        );
    }

    /**
     * Offset offset conversion.
     *
     * @param offset - the offset
     * @return the offset conversion
     */
    @Nonnull
    public static OffsetConversion offset(double offset) {
        return new OffsetConversion.DoubleOffset(offset);
    }

    /**
     * Helper method to create a offset conversion from a integer;
     *
     * @param offset - the offset
     * @return the offset conversion
     */
    @Nonnull
    public static OffsetConversion offset(@Nonnull BigDecimal offset) {
        return new OffsetConversion.DecimalOffset(offset);
    }

    /**
     * Coefficient then offset linear conversion.
     *
     * @param coefficient the coefficient
     * @param offset      the offset
     * @return the linear conversion
     */
    @Nonnull
    public static LinearConversion coefficientThenOffset(double coefficient, double offset) {
        if (coefficient == 1 && offset == 0) {
            return identity();
        }
        return DoubleLinearOffsetConversion.coefficientThenOffset(coefficient, offset);
    }

    /**
     * Offset then coefficient linear conversion.
     *
     * @param offset      the offset
     * @param coefficient the coefficient
     * @return the linear conversion
     */
    @Nonnull
    public static LinearConversion offsetThenCoefficient(double offset, double coefficient) {
        if (coefficient == 1 && offset == 0) {
            return identity();
        }
        return DoubleLinearOffsetConversion.offsetThenCoefficient(offset, coefficient);
    }

    /**
     * Coefficient then offset linear conversion.
     *
     * @param linear the linear
     * @param offset the offset
     * @return the linear conversion
     */
    @Nonnull
    public static LinearConversion coefficientThenOffset(
            @Nonnull MultiplicativeConversion linear, @Nonnull OffsetConversion offset) {
        return LinearOffsetConversion.coefficientThenOffset(linear, offset);
    }

    /**
     * Offset then coefficient linear conversion.
     *
     * @param offset the offset
     * @param linear the linear
     * @return the linear conversion
     */
    @Nonnull
    public static LinearConversion offsetThenCoefficient(
            @Nonnull OffsetConversion offset, @Nonnull MultiplicativeConversion linear
    ) {
        return LinearOffsetConversion.offsetThenCoefficient(offset, linear);
    }

    /**
     * Helper method to create a Factor conversion from base^exponent
     *
     * @param base     the base
     * @param exponent - the exponent
     * @return the factor conversion
     * @see #multiply(double) #multiply(double)
     */
    @Nonnull
    public static MultiplicativeConversion power(int base, int exponent) {
        if (exponent == 0 || base == 1) {
            return identity();
        }
        return new PowerConversion(base, exponent);
    }

    /**
     * Returns a logarithmic conversion of log(base).
     *
     * @param base the base
     * @return the logarithmic conversion
     * @throws ArithmeticException if base is <= 0 or 1
     */
    @Nonnull
    public static LogExpConversion log(double base) throws ArithmeticException {
        if (base <= 0.0 || base == 1.0) {
            throw new ArithmeticException("log of base: " + base + "is not valid");
        }
        return LogExpConversion.log(base);
    }

    /**
     * Returns a exponential conversion (base^x).
     *
     * @param base the exponential base
     * @return the exponential conversion
     * @throws ArithmeticException if base is <= 0 or 1
     */
    @Nonnull
    public static LogExpConversion exponential(double base) throws ArithmeticException {
        if (base <= 0.0 || base == 1.0) {
            throw new ArithmeticException("log of base: " + base + "is not valid");
        }
        return LogExpConversion.exp(base);
    }

    /**
     * Helper method to create a flattened conversion with flatten as <code>true</code>
     *
     * @param conversions the conversions array
     * @return a conversion of: factor of 1 if <code>conversions</code> is empty,
     * the factor passed if the <code>conversions</code> is length 1,
     * <code>CompositeConversion</code> if <code>conversions</code> is longer than 1.
     */
    @Nonnull
    public static UnitConversion of(@Nonnull UnitConversion... conversions) {
        if (conversions.length == 0) {
            return identity();
        }
        if (conversions.length == 1) {
            return conversions[0];
        }
        boolean isLinear = true;
        for (int i = 0; i < conversions.length; i++) {
            if (!(conversions[i] instanceof MultiplicativeConversion)) {
                isLinear = false;
                break;
            }
        }
        if (isLinear) {
            MultiplicativeConversion[] multiplicativeConversions =
                    Arrays.copyOf(conversions, conversions.length, MultiplicativeConversion[].class);

            return foldDouble(multiplicativeConversions);
        }
        if (conversions.length == 2) {
            return new PairConversion(conversions[0], conversions[1]);
        }

        return new CompositeConversion(conversions);
    }

    /**
     * Helper method to create a single <code>FactorConversion</code>
     * from an array of <code>FactorConversion</code>
     *
     * @param multiConversions the multiplicative conversions
     * @return the factor conversion
     */
    @Nonnull
    public static MultiplicativeConversion foldDouble(MultiplicativeConversion... multiConversions) {
        double result = 1;
        for (int i = 0, length = multiConversions.length; i < length; i++) {
            result *= multiConversions[i].getCoefficient();
        }
        return multiply(result);
    }

    /**
     * Fold decimal multiplicative conversion.
     *
     * @param multiConversions the multi conversions
     * @return the multiplicative conversion
     */
    @Nonnull
    public static MultiplicativeConversion foldDecimal(MultiplicativeConversion... multiConversions) {
        BigDecRational result = BigDecRational.ONE;
        for (int i = 0, length = multiConversions.length; i < length; i++) {
            result = result.multiply(multiConversions[i].getRationalCoefficient());
        }
        return multiply(result);
    }

}
