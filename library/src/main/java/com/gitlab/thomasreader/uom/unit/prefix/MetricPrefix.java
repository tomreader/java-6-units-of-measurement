/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit.prefix;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.PrefixedUnit;
import com.gitlab.thomasreader.uom.unit.Unit;

import javax.annotation.Nonnull;

/**
 * <p>
 * Models a <a href="https://en.wikipedia.org/wiki/Metric_prefix">metric prefix</a>
 * that precedes a basic unit of measure to indicate a multiple or fraction of the unit.
 * </p>
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Metric_prefix">Wikipedia - Metric prefix</a>
 * @since 0.1.0
 */
public enum MetricPrefix implements UnitPrefix {
    
    /**
     * 10<sup>24</sup>
     */
    YOTTA("Y", 24),
    /**
     * 10<sup>21</sup>
     */
    ZETTA("Z", 21),
    /**
     * 10<sup>18</sup>
     */
    EXA("E", 18),
    /**
     * 10<sup>15</sup>
     */
    PETA("P", 15),
    /**
     * 10<sup>12</sup>
     */
    TERA("T", 12),
    /**
     * 10<sup>9</sup>
     */
    GIGA("G", 9),
    /**
     * 10<sup>6</sup>
     */
    MEGA("M", 6),
    /**
     * 10<sup>3</sup>
     */
    KILO("k", 3),
    /**
     * 10<sup>2</sup>
     */
    HECTO("h", 2),
    /**
     * 10<sup>1</sup>
     */
    DEKA("da", 1),
    /**
     * 10<sup>-1</sup>
     */
    DECI("d", -1),
    /**
     * 10<sup>-2</sup>
     */
    CENTI("c", -2),
    /**
     * 10<sup>-3</sup>
     */
    MILLI("m", -3),
    /**
     * 10<sup>-6</sup>
     */
    MICRO("\u00B5", -6), // μ \u00C2 \u00B5
    /**
     * 10<sup>-9</sup>
     */
    NANO("n", -9),
    /**
     * 10<sup>-12</sup>
     */
    PICO("p", -12),
    /**
     * 10<sup>-15</sup>
     */
    FEMTO("f", -15),
    /**
     * 10<sup>-18</sup>
     */
    ATTO("a", -18),
    /**
     * 10<sup>-21</sup>
     */
    ZEPTO("z", -21),
    /**
     * 10<sup>-24</sup>
     */
    YOCTO("y", -24);

    @Nonnull
    private String symbol;
    private int exponent;
    
    MetricPrefix(@Nonnull String symbol, int exponent) {
        this.symbol = symbol;
        this.exponent = exponent;
    }
    
    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getSymbol() {
        return this.symbol;
    }
    
    /**
     * {@inheritDoc}
     * @implNote returns 10<sup>exponent</sup>
     */
    @Nonnull
    @Override
    public MultiplicativeConversion getConversion() {
        return Conversions.power(10, this.exponent);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Quantity<T>> Unit<T> prefix(@Nonnull Unit<T> unit) {
        try {
            return this.prefixOrThrow(unit);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Nonnull
    @Override
    public <T extends Quantity<T>> Unit<T> prefixOrThrow(@Nonnull Unit<T> unit) throws IllegalArgumentException {
//        if (unit instanceof DerivedUnit) {
//            throw new IllegalArgumentException("Derived units cannot be prefixed");
//        } else if (unit instanceof PrefixedUnit) {
//            throw new IllegalArgumentException("Compound prefixes are not permitted");
//        }
        if (!unit.canPrefix()) {
            throw new IllegalArgumentException("This unit is not permitted to be prefixed");
        }
        return new PrefixedUnit<T>(this, unit);
    }


    public static MetricPrefix getPrefixFor(double value) {
        double log10 = Math.log10(value);
        if (log10 < -21.0) return YOCTO;
        if (log10 < -18.0) return ZEPTO;
        if (log10 < -15.0) return ATTO;
        if (log10 < -12.0) return FEMTO;
        if (log10 < -9.0) return PICO;
        if (log10 < -6.0) return NANO;
        if (log10 < -3.0) return MICRO;
        if (log10 < -1.0) return MILLI;
        if (log10 < 1.0) return CENTI;
        if (log10 < 3.0) return null;
        if (log10 < 6.0) return KILO;
        if (log10 < 9.0) return MEGA;
        if (log10 < 12.0) return GIGA;
        if (log10 < 15.0) return TERA;
        if (log10 < 18.0) return PETA;
        if (log10 < 21.0) return EXA;
        if (log10 < 24.0) return ZETTA;
        else return YOTTA;
    }
}
