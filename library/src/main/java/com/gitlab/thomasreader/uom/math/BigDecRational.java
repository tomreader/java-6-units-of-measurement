/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * Represents a rational number as a fraction where the numerator (dividend) and
 * denominator (divisor) are both integers — in this implementation they are modeled using
 * {@link BigDecimal} for performance.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="http://en.wikipedia.org/wiki/Rational_number">Wikipedia: Rational number</a>
 * @since 0.1.0
 */
public class BigDecRational extends Number implements Comparable<BigDecRational> {

    /**
     * The BigRational constant zero. (0 = 0/1)
     */
    @Nonnull
    public static final BigDecRational ZERO = new BigDecRational(BigDecimal.ZERO);

    /**
     * The BigRational constant one. (1 = 1/1)
     */
    @Nonnull
    public static final BigDecRational ONE = new BigDecRational(BigDecimal.ONE);

    /**
     * The BigRational constant ten. (10 = 10/1)
     */
    @Nonnull
    public static final BigDecRational TEN = new BigDecRational(BigDecimal.TEN);


    @Nonnull
    private final BigDecimal numerator;
    @Nonnull
    private final BigDecimal denominator;
    // cached hash code
    private final int hashCode;


    /**
     * Constructs a RationalNumber equal to {@code numerator / denominator}.
     *
     * @param numerator the numerator {@link BigDecimal} value
     * @param denominator the denominator {@link BigDecimal} value
     * @throws ArithmeticException if the dominator is zero
     */
    public BigDecRational(@Nonnull BigDecimal numerator, @Nonnull BigDecimal denominator) throws ArithmeticException {
        BigDecimal num = numerator;
        BigDecimal den = denominator;

        // Throw if denominator is zero
        if (den.signum() == 0) {
            throw new ArithmeticException("BigRational divide by zero");
        }

        // If denominator is negative then multiply both numerator and denominator by -1 (negate)
        if (den.signum() < 0) {
            num = num.negate();
            den = den.negate();
        }

        this.hashCode = 31 * (31 + num.hashCode()) + den.hashCode();
        this.numerator = num;
        this.denominator = den;
    }

    private BigDecRational(@Nonnull BigDecimal numerator) {
        this(numerator, BigDecimal.ONE);
    }

    @Nonnull
    public static BigDecRational valueOf(@Nonnull BigDecimal numerator,
                                         @Nonnull BigDecimal denominator) throws ArithmeticException {
        if (denominator.signum() == 0) {
            throw new ArithmeticException("BigRational divide by zero");
        }
        if (numerator.signum() == 0) {
            return BigDecRational.ZERO;
        }
        if (numerator.compareTo(denominator) == 0) {
            return BigDecRational.ONE;
        }
        if (denominator.compareTo(BigDecimal.ONE) == 0) {
            if (numerator.compareTo(BigDecimal.TEN) == 0) {
                return BigDecRational.TEN;
            }
        }

//        int scale = numerator.scale() - denominator.scale();
//        int numScaleAdjust = (scale >= 0) ? numerator.scale() : numerator.scale() - scale;
//        int denScaleAdjust = (scale <= 0) ? denominator.scale() : denominator.scale() + scale;
//
//        BigDecimal num = numerator.scaleByPowerOfTen(numScaleAdjust);
//        BigDecimal den = denominator.scaleByPowerOfTen(denScaleAdjust);
        BigDecimal num = numerator;
        BigDecimal den = denominator;

        return new BigDecRational(num, den);

        //return new BigDecimalRational(numerator, denominator);
    }

    /**
     * Returns a BigRational equal to {@code numerator / denominator}. This static
     * factory method is preferred to {@link BigRational#BigRational(BigInteger, BigInteger)} for
     * reuse of BigRationals.
     *
     * @param numerator the numerator {@link BigInteger} value
     * @param denominator the denominator {@link BigInteger} value
     * @return the rational number {@code numerator / denominator}
     * @throws ArithmeticException if the dominator is zero
     */
    @Nonnull
    public static BigDecRational valueOf(@Nonnull BigInteger numerator,
                                         @Nonnull BigInteger denominator) throws ArithmeticException {
        if (denominator.signum() == 0) {
            throw new ArithmeticException("BigRational divide by zero");
        }
        if (numerator.signum() == 0) {
            return BigDecRational.ZERO;
        }
        if (numerator.compareTo(denominator) == 0) {
            return BigDecRational.ONE;
        }
//        if (denominator.compareTo(BigInteger.ONE) == 0) {
//            if (numerator.compareTo(BigInteger.TEN) == 0) {
//                return BigDecRational.TEN;
//            }
//        }
        return new BigDecRational(new BigDecimal(numerator), new BigDecimal(denominator));
    }

    /**
     * Returns a BigRational equal to {@code numerator / denominator}.
     *
     * @param numerator the numerator {@code long} value
     * @param denominator the denominator {@code long} value
     * @return the rational number {@code numerator / denominator}
     * @throws ArithmeticException if the dominator is zero
     */
    @Nonnull
    public static BigDecRational valueOf(long numerator, long denominator) throws ArithmeticException {
        if (denominator == 0) {
            throw new ArithmeticException("BigRational divide by zero");
        }
        if (numerator == 0) {
            return BigDecRational.ZERO;
        }
        if (numerator == denominator) {
            return BigDecRational.ONE;
        }
        if (numerator == 10 && denominator == 1) {
            return BigDecRational.TEN;
        }
        return new BigDecRational(BigDecimal.valueOf(numerator), BigDecimal.valueOf(denominator));
    }

    /**
     * Returns a BigRational whose value is equal to {@code integer / 1}.
     *
     * @param integer the integer {@link BigInteger} value
     * @return the rational number {@code integer / 1}
     */
    @Nonnull
    public static BigDecRational valueOf(@Nonnull BigInteger integer) {
        if (integer.compareTo(BigInteger.ZERO) == 0) {
            return BigDecRational.ZERO;
        }
        if (integer.compareTo(BigInteger.ONE) == 0) {
            return BigDecRational.ONE;
        }
        if (integer.compareTo(BigInteger.TEN) == 0) {
            return BigDecRational.TEN;
        }
        return new BigDecRational(new BigDecimal(integer));
    }

    /**
     * Returns a BigRational whose value is equal to {@code integer / 1}.
     *
     * @param integer the integer {@link Long} value
     * @return the rational number {@code integer / 1}
     */
    @Nonnull
    public static BigDecRational valueOf(long integer) {
        return valueOf(BigDecimal.valueOf(integer));
    }

    /**
     * Returns a BigRational whose: numerator is equal to the unscaled value of the {@code decimal}
     * and denominator is equal to ten to the power of the scale of the {@code decimal}.
     *
     * @param decimal the decimal {@link BigDecimal} value
     * @return the rational number {@code decimal.unscaledValue() / ten^decimal.scale()}
     * @see BigDecimal#unscaledValue()
     * @see BigDecimal#scale()
     */
    @Nonnull
    public static BigDecRational valueOf(@Nonnull BigDecimal decimal) {
        return valueOf(decimal, BigDecimal.ONE);
    }

    /**
     * Convenience method to return a BigRational from a {@code double}. Internally uses
     * {@link #valueOf(BigDecimal)}, {@link Double#toString(double)} and
     * {@link BigDecimal#BigDecimal(String)}.
     *
     * @param decimal the decimal {@link Double} value
     * @return the rational number
     * @see BigDecimal#BigDecimal(String)
     * @see Double#toString(double)
     * @see #valueOf(BigDecimal)
     */
    @Nonnull
    public static BigDecRational valueOf(double decimal) {
        return valueOf(new BigDecimal(Double.toString(decimal)));
    }

    /**
     * Returns the numerator of this rational number.
     *
     * @return the numerator
     */
    @Nonnull
    public BigDecimal getNumerator() {
        return this.numerator;
    }

    /**
     * Returns the denominator of this rational number.
     *
     * @return the denominator
     */
    @Nonnull
    public BigDecimal getDenominator() {
        return this.denominator;
    }

    /**
     * Converts this BigRational to an {@code int}. This conversion is analogous to {@code int}
     * division. If the BigRational is a proper fraction (
     * <a href="https://en.wikipedia.org/wiki/Fraction#Proper_and_improper_fractions">Wikipedia: Fraction</a>
     * ) then this conversion will return zero. If this BigRational can be represented as a mixed
     * fraction then this conversion will return the whole number part. Information can be lost
     * if the whole number part is greater than the size of a {@code int}.
     *
     * @return the integer part of this rational number
     * @see BigInteger#divide(BigInteger)
     * @see BigInteger#intValue()
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Mixed_numbers">
     *     Wikipedia: Fraction - Mixed Numbers</a>
     */
    @Override
    public int intValue() {
        return this.bigIntegerValue().intValue();
        //return this.getNumerator().divide(this.getDenominator()).intValue();
    }

    /**
     * Converts this BigRational to an {@code long}. This conversion is analogous to {@code long}
     * division. If the BigRational is a proper fraction (
     * <a href="https://en.wikipedia.org/wiki/Fraction#Proper_and_improper_fractions">Wikipedia: Fraction</a>
     * ) then this conversion will return zero. If this BigRational can be represented as a mixed
     * fraction then this conversion will return the whole number part. Information can be lost
     * if the whole number part is greater than the size of a {@code long}.
     *
     * @return the integer part of this rational number
     * @see BigInteger#divide(BigInteger)
     * @see BigInteger#longValue()
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Mixed_numbers">
     *     Wikipedia: Fraction - Mixed Numbers</a>
     */
    @Override
    public long longValue() {
        return this.bigIntegerValue().longValue();
        //return this.getNumerator().divide(this.getDenominator()).longValue();
    }

    /**
     * Converts this BigRational to an {@code BigInteger}. The conversion divides the numerator by
     * the demoniator and analogous to {@code int} division. If the BigRational is a proper fraction (
     * <a href="https://en.wikipedia.org/wiki/Fraction#Proper_and_improper_fractions">Wikipedia: Fraction</a>
     * ) then this conversion will return zero. If this BigRational can be represented as a mixed
     * fraction then this conversion will return the whole number part.
     *
     * @return the integer part of this BigRational
     * @see BigInteger#divide(BigInteger)
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Mixed_numbers">
     *     Wikipedia: Fraction - Mixed Numbers</a>
     */
    @Nonnull
    public BigInteger bigIntegerValue() {
        return this.getNumerator().divide(this.getDenominator(), MathContext.DECIMAL128).toBigInteger();
    }

    /**
     * Converts this BigRational to a {@code float}. If either the numerator or denominator are too
     * large to fit in a {@code float} then they will be represented by
     * {@link Float#POSITIVE_INFINITY} or {@link Float#NEGATIVE_INFINITY}. This conversion can lose
     * information about the precision of this BigRational.
     *
     * @return this BigRational converted to a {@code float}
     * @see BigInteger#floatValue()
     */
    @Override
    public float floatValue() {
        return this.getNumerator().floatValue() / this.getDenominator().floatValue();
    }

    /**
     * Converts this BigRational to a {@code double}. If either the numerator or denominator are too
     * large to fit in a {@code double} then they will be represented by
     * {@link Double#POSITIVE_INFINITY} or {@link Double#NEGATIVE_INFINITY}. This conversion can lose
     * information about the precision of this BigRational.
     *
     * @return this BigRational converted to a {@code double}
     * @see BigInteger#doubleValue()
     */
    @Override
    public double doubleValue() {
        return this.getNumerator().doubleValue() / this.getDenominator().doubleValue();
    }

    /**
     * Converts this BigRational to a {@link BigDecimal}. Calls {@link #bigDecimalValue(MathContext)}
     * using {@link MathContext#DECIMAL128}.
     *
     * @return the BigRational converted to a {@link BigDecimal}
     * @see #bigDecimalValue(MathContext)
     */
    @Nonnull
    public BigDecimal bigDecimalValue() {
        return this.bigDecimalValue(MathContext.DECIMAL128);
    }


    /**
     * Converts this BigRational to a {@link BigDecimal}.The mathContext is used in the division of
     * the numerator by the denominator.
     *
     * @param mathContext the MathContext used in the division
     * @return the BigRational converted to a {@link BigDecimal}
     * @see BigDecimal#divide(BigDecimal, MathContext)
     */
    @Nonnull
    public BigDecimal bigDecimalValue(@Nonnull MathContext mathContext) {
        if (this.getDenominator().compareTo(BigDecimal.ONE) == 0) {
            return this.getNumerator().round(mathContext);
        }
        return this.getNumerator().divide(this.getDenominator(), mathContext);
    }

    /**
     * Simplifies this BigRational to a simplified and equivalent BigRational. If this BigRational
     * is already in its lowest terms then this is returned; else the greatest common factor is
     * identified and the numerator and denominator are both divided by it.
     *
     * @return an equivalent BigRational in its lowest possible terms
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Simplifying_(reducing)_fractions">
     *     Wikipedia: Fraction</a>
     * @see <a href="https://en.wikipedia.org/wiki/Greatest_common_divisor">
     *     Wikipedia: Greatest common divisor</a>
     */
    @Nonnull
    public BigDecRational simplify() {
        if (this.getNumerator().compareTo(BigDecimal.ONE) == 0 ||
            this.getDenominator().compareTo(BigDecimal.ONE) == 0) {
            return this;
        }
        BigInteger intNumerator = this.getNumerator().toBigInteger();
        BigInteger intDenominator = this.getDenominator().toBigInteger();
        BigInteger gcd = intNumerator.gcd(intDenominator);
        if (gcd.equals(BigInteger.ONE)) {
            return this;
        } else {
            BigInteger num = intNumerator.divide(gcd);
            BigInteger den = intDenominator.divide(gcd);
            return valueOf(num, den);
        }
    }

    /**
     * Returns the signum of this BigRational.
     *
     * @return -1, 0 or 1 as the value of this BigRational is negative, zero or positive.
     */
    public int signum() {
        return this.getNumerator().signum() / this.getDenominator().signum();
    }

    /**
     * Returns a BigRational whose value is ({@code -this}).
     *
     * @return {@code -this}
     */
    @Nonnull
    public BigDecRational negate() {
        return valueOf(this.getNumerator().negate(), this.getDenominator());
    }


    /**
     * Returns whether this BigRational is zero.
     *
     * @return {@code true} if this equals zero (0/1), {@code false} otherwise
     */
    public boolean isZero() {
        return this.signum() == 0;
    }

    /**
     * Returns the reciprocal of this BigRational. The returned BigRational should equal
     * denominator / numerator.
     *
     * @return the reciprocal BigRational (denominator / numerator)
     * @throws ArithmeticException if {@code this} equals zero (0/1)
     */
    @Nonnull
    public BigDecRational reciprocal() throws ArithmeticException {
        if (this.signum() < 0) {
            return valueOf(this.getDenominator().negate(), this.getNumerator().negate());
        }
        return valueOf(this.getDenominator(), this.getNumerator());
    }

    /**
     * Returns whether or not this BigRational can be represented as an integer.
     *
     * @return {@code true} if {@code numerator % denominator == 0}, {@code false} otherwise
     */
    public boolean isInteger() {
        return this.thisIsInteger() || this.simplify().thisIsInteger();
    }

    private boolean thisIsInteger() {
        return this.getDenominator().compareTo(BigDecimal.ONE) == 0;
    }

    /**
     * Returns whether this BigRational is a proper fraction.
     *
     * @return {@code true} if numerator divided by denominator is greater than -1 or less than 1,
     *  {code false} otherwise.
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Proper_and_improper_fractions">Wikipedia: Fraction</a>
     */
    public boolean isProper() {
        return this.getNumerator().compareTo(this.getDenominator()) < 0;
    }

    /**
     * Returns the whole number part of this BigRational.
     *
     * @return the integer part of this BigRational
     * @see #bigIntegerValue()
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Mixed_numbers">
     *     Wikipedia: Fraction - Mixed Numbers</a>
     */
    @Nonnull
    public BigInteger getIntegerPart() {
        return this.bigIntegerValue();
    }

    /**
     * Returns the proper fraction part of this BigRational. The result BigRational is equal to
     * {@code this - this.getIntegerPart()}
     *
     * @return the fraction part of this BigRational
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Mixed_numbers">
     *     Wikipedia: Fraction - Mixed Numbers</a>
     */
    @Nonnull
    public BigDecRational getProperFractionPart() {
        if (this.isProper()) {
            return this;
        }

        return new BigDecRational(this.getNumerator().remainder(this.getDenominator()), this.getDenominator());
    }

    /**
     * Returns a {@code BigRational} whose value is equal to the absolute value of this
     * {@code BigRational}. Returns {@code this} if this is positive.
     *
     * @return {@code abs(this)}
     */
    @Nonnull
    public BigDecRational abs() {
        if (this.signum() == -1) {
            return new BigDecRational(this.getNumerator().abs(), this.getDenominator());
        } else {
            return this;
        }
    }

    /**
     * Returns a BigRational equal to {@code this + 1}
     *
     * @return {@code this + 1}
     */
    @Nonnull
    public BigDecRational inc() {
        return valueOf(this.getNumerator().add(this.getDenominator()), this.getDenominator());
    }

    /**
     * Returns a BigRational equal to {@code this - 1}
     *
     * @return {@code this - 1}
     */
    @Nonnull
    public BigDecRational dec() {
        return valueOf(this.getNumerator().subtract(this.getDenominator()), this.getDenominator());
    }

    /**
     * Returns a BigRational equal to {@code this + addend}.
     *
     * @param addend the BigRational to add
     * @return {@code this + addend}
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Addition">
     *     Wikipedia: Fraction - Addition</a>
     */
    @Nonnull
    public BigDecRational add(@Nonnull BigDecRational addend) {
        if (addend.equals(BigDecRational.ZERO)) {
            return this;
        }

        // same denominators
        if (this.getDenominator().compareTo(addend.getDenominator()) == 0) {
            return valueOf(this.getNumerator().add(addend.getNumerator()), this.getDenominator());
        }

        // unlike denominator -> a/b + c/d = (ad + cb) / bd
        return valueOf(
                this.getNumerator().multiply(addend.getDenominator())
                        .add(addend.getNumerator().multiply(this.getDenominator())),
                this.getDenominator().multiply(addend.getDenominator()));
    }

    /**
     * Returns a BigRational equal to {@code this - subtrahend}.
     *
     * @param subtrahend the BigRational to subtract
     * @return {@code this - subtrahend}
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Subtraction">
     *     Wikipedia: Fraction - Subtraction</a>
     */
    @Nonnull
    public BigDecRational subtract(@Nonnull BigDecRational subtrahend) {
        if (subtrahend.equals(BigDecRational.ZERO)) {
            return this;
        }

        return this.add(subtrahend.negate());
    }

    /**
     * Returns a BigRational equal to {@code this * multiplicand}.
     *
     * @param multiplicand the BigRational to multiply
     * @return {@code this * multiplicand}
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Multiplication">
     *     Wikipedia: Fraction - Multiplication</a>
     */
    @Nonnull
    public BigDecRational multiply(@Nonnull BigDecRational multiplicand) {
        // number x: x * 0 = 0
        if (this.signum() * multiplicand.signum() == 0) {
            return BigDecRational.ZERO;
        }

        // number x: x * 1 = x
        if (multiplicand.equals(BigDecRational.ONE)) {
            return this;
        }

        BigDecimal num = this.getNumerator().multiply(multiplicand.getNumerator(), MathContext.DECIMAL128);
        BigDecimal den = this.getDenominator().multiply(multiplicand.getDenominator(), MathContext.DECIMAL128);

        return valueOf(num, den);
    }

    /**
     * Returns a BigRational equal to {@code this / dividend}. {@code (a/b) / (c/d) == (a/b) * (d/c)}
     *
     * @param dividend the BigRational to divide
     * @return {@code this / dividend}
     * @see <a href="https://en.wikipedia.org/wiki/Fraction#Division">
     *     Wikipedia: Fraction - Division</a>
     */
    @Nonnull
    public BigDecRational divide(@Nonnull BigDecRational dividend) {
        if (dividend.equals(BigDecRational.ONE)) {
            return this;
        }

        return this.multiply(dividend.reciprocal());
    }

    /**
     * Returns a BigRational equal to {code this^exponent}. When {@code exponent} is: greater than
     * zero then returns a BigRational equal to {@code (a/b)^exponent == a^exponent / b^exponent};
     * equal to zero then returns a {@link #ONE}; less than zero then returns a BigRational equal to
     * {@code (a/b)^exponent == b^-exponent/a^-exponent}
     *
     * @param exponent the exponent to raise {@code this} to the power of
     * @return {@code (numerator / denominator) ^exponent}
     */
    @Nonnull
    public BigDecRational pow(int exponent) {
        int signum = this.signum();

        if (exponent == 0) {
            if (signum == 0) {
                throw new ArithmeticException("0^0 is not defined");
            }
            return BigDecRational.ONE;
        }

        if (signum == 0) {
            return BigDecRational.ZERO;
        }

        if (exponent > 0) {
            return new BigDecRational(this.getNumerator().pow(exponent), this.getDenominator().pow(exponent));
        } else {
            return new BigDecRational(this.getDenominator().pow(-exponent), this.getNumerator().pow(-exponent));
        }
    }

    /**
     * Returns the largest of two rational numbers.
     *
     * @param other the rational number to compare with
     * @return the largest BigRational between {@code this} and {@code other}
     */
    @Nonnull
    public BigDecRational max(@Nonnull BigDecRational other) {
        return this.compareTo(other) < 0 ? other : this;
    }

    /**
     * Returns the largest of {@code 1 + others.length} rational numbers.
     *
     * @param others the rational numbers to compare with
     * @return the largest BigRational between {@code this} and each BigRational in {@code others}
     */
    @Nonnull
    public BigDecRational max(@Nonnull BigDecRational... others) {
        BigDecRational largestVal = this;
        for (BigDecRational other : others) {
            largestVal = largestVal.max(other);
        }
        return largestVal;
    }

    /**
     * Returns the smallest of two rational numbers.
     *
     * @param other the rational number to compare with
     * @return the smallest BigRational between {@code this} and {@code other}
     */
    @Nonnull
    public BigDecRational min(@Nonnull BigDecRational other) {
        return this.compareTo(other) > 0 ? other : this;
    }

    /**
     * Returns the smallest of {@code 1 + others.length} rational numbers.
     *
     * @param others the rational numbers to compare with
     * @return the smallest BigRational between {@code this} and each BigRational in {@code others}
     */
    @Nonnull
    public BigDecRational min(@Nonnull BigDecRational... others) {
        BigDecRational smallestVal = this;
        for (BigDecRational other : others) {
            smallestVal = smallestVal.min(other);
        }
        return smallestVal;
    }

    /**
     * Compares this BigRational with the specified BigRational. Two BigRational objects which are
     * equal in value but have different numerators and denominators are considered equal
     * (i.e. 1/2 and 2/4 simplified both equal 1/2).
     *
     * @param other the {@code BigRational} to which this {@code BigRational} is to be compared
     * @return -1, 0 or 1 when this {@code BigRational} is less than, equal to or greater than {@code other}
     */
    @Override
    public int compareTo(@Nonnull BigDecRational other) {
        if (this.equals(other)) {
            return 0;
        }

        if (this.getDenominator().compareTo(other.getDenominator()) == 0) {
            return this.getNumerator().compareTo(other.getNumerator());
        }

        return this.bigDecimalValue().compareTo(other.bigDecimalValue());
    }

    /**
     * Returns the string representation of this {@code BigRational}.
     *
     * If this {@code BigRational} has a denominator equal to one then only the string
     * representation of the numerator will be returned.
     *
     * Otherwise the numerator and denominator will be returned using {@code '/'} to symbolise
     * the division between the numerator and denominator.
     *
     * BigRationals which can be simplified to integers will be returned as a fraction.
     *
     * <b>Examples;</b>
     * For each representation [<i>numerator / denominator</i>] on the left, the resulting string is
     * shown on the right.
     *
     * {@code [0 / 1]       "0"}
     * {@code [1 / 1]       "1"}
     * {@code [3 / 3]       "3/3"}
     * {@code [1 / 3]       "1/3"}
     *
     * @return the string representation of this {@code BigRational}
     * @see BigInteger#toString()
     */
    @Override
    @Nonnull
    public String toString() {
        return this.toString('/');
    }

    /**
     * Returns the string representation of this {@code BigRational}.
     *
     * If this {@code BigRational} has a denominator equal to one then only the string
     * representation of the numerator will be returned.
     *
     * Otherwise the numerator and denominator will be returned using {@code divisionSymbol}
     * to symbolise the division between the numerator and denominator.
     *
     * BigRationals which can be simplified to integers will be returned as a fraction.
     *
     * <b>Examples;</b>
     * For each representation [<i>numerator, {@code divisionSymbol} ,denominator</i>] on the left,
     * the resulting string is shown on the right.
     *
     * {@code [0, /, 1]       "0"}
     * {@code [1, /, 1]       "1"}
     * {@code [3, /, 3]       "3/3"}
     * {@code [1, ÷, 3]       "1÷3"}
     *
     *
     * @param divisionSymbol the symbol to use to symbolise the division in this {@code BigRational}
     * @return the string representation of this {@code BigRational}
     */
    @Nonnull
    public String toString(char divisionSymbol) {
        if (this.thisIsInteger()) {
            return this.numerator.toString();
        } else {
            return this.numerator.toString() + divisionSymbol + this.denominator.toString();
        }
    }

    /**
     * Returns the decimal string representation of this {@code BigRational}.
     *
     * @return the decimal string representation of this {@code BigRational}
     * @see BigDecimal#toString()
     */
    @Nonnull
    public String toDecimalString() {
        return this.bigDecimalValue().toString();
    }


    /**
     * Returns the hash code for this {@code BigRational}.
     *
     * @return the hash code for this {@code BigRational}
     */
    @Override
    public int hashCode() {
        return this.hashCode;
    }

    /**
     * Compares this {@code BigRational} with the specified Object for equality.
     *
     * This method does not take into account simplified equality: a rational number of 1/2
     * will not be considered equal to a rational number of 2/4 by this method despite both
     * having an equal value.
     *
     * @param o the Object to compare to this {@code BigRational}
     * @return true if the specified Object is a {@code BigRational} whose numerator and denominator
     * are equal
     * @see BigInteger#equals(Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BigDecRational)) {
            return false;
        }

        BigDecRational other = (BigDecRational) o;

        if (!this.getNumerator().equals(other.getNumerator())) {
            return false;
        }

        return this.getDenominator().equals(other.getDenominator());
        //return this.bigDecimalValue().equals(other.bigDecimalValue());
    }
}
