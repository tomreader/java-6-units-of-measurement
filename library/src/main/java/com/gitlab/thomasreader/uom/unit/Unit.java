/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.quantity.factory.QuantityFactory;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.quantity.type.Mass;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;
import com.gitlab.thomasreader.uom.unit.prefix.UnitPrefix;

import javax.annotation.Nonnull;

/**
 * <p>
 * Models a <a href="https://en.wikipedia.org/wiki/Unit_of_measurement">unit of measurement</a>.
 * </p>
 * <p>
 * <i>The metre is a unit of length that represents a definite predetermined length.
 * When we say 10 metres (or 10 m), we actually mean 10 times the definite predetermined length
 * called "metre". Measurement is a process of determining how large or small a physical quantity
 * is as compared to a basic reference quantity of the same kind.</i>
 * </p>
 *
 * @param <T> the type of quantity being measured: common are {@linkplain Mass Mass}
 *           and {@linkplain Length Length}
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Unit_of_measurement">Wikipedia - Unit of measurement</a>
 * @since 0.1.0
 */
public interface Unit<T extends Quantity<T>> extends QuantityFactory<T> {//, Comparable<Unit<T>> {

    /**
     * Returns a string representation of the unit symbol.
     *
     * @return the symbol
     */
    public String getSymbol();

    /**
     * Returns the symbol format.
     *
     * @return the symbol format
     */
    @Nonnull
    public UnitSymbolFormat getSymbolFormat();

    /**
     * Gets the type of quantity.
     *
     * @return the type of quantity
     */
    @Nonnull
    public Class<T> getType();

    /**
     * Gets the conversion to convert between {@code this} and the {@link ReferenceUnit}.
     *
     * @return the conversion
     */
    @Nonnull
    public UnitConversion getConversion();

    /**
     * Gets the reference unit which this is relative to.
     *
     * @return the reference unit
     */
    @Nonnull
    public Unit<T> getReferenceUnit();

    /**
     * Whether this unit can be prefixed by a {@link UnitPrefix}
     *
     * @return the boolean
     */
    public boolean canPrefix();
}
