/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.unit.Unit;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nonnull;

/**
 * Compound quantity useful for generating quantities such as 6'0" or 5h 25m 12s.
 *
 * @param <T> the quantity type
 * @author Tom Reader
 * @version 0.1.0
 * @since   0.1.0
 */
public class CompoundQuantity<T extends Quantity<T>> {

    private Quantity<T> originalQuantity;
    @Nonnull
    private final Quantity<T>[] quantities;
    private String separator = " ";


    /**
     * Instantiates a new Compound quantity.
     *
     * @param quantities the quantities
     * @throws IllegalArgumentException if quantities contains less than 2 quantities
     */
    public static <T extends Quantity<T>> CompoundQuantity<T> of(@Nonnull Quantity<T>... quantities) throws IllegalArgumentException {
        return new CompoundQuantity<T>(null, quantities);
    }

    public static <T extends Quantity<T>> CompoundQuantity<T> of(@Nonnull Quantity<T> quantity, int decPlaces, Unit<T>... units) throws IllegalArgumentException {
        Map<Double, Unit<T>> multiplicativeUnits = new TreeMap<Double, Unit<T>>(new Comparator<Double>() {
            @Override
            public int compare(Double a, Double b) {
                return -Double.compare(a, b);
            }
        });
        for (int i = 0; i < units.length; i++) {
            Unit<T> iunit = units[i];
            if (iunit.getConversion() instanceof MultiplicativeConversion) {
                multiplicativeUnits.put(
                        ((MultiplicativeConversion) iunit.getConversion()).getCoefficient(),
                        iunit);
            }
        }
        if (multiplicativeUnits.size() > 1) {
            Unit<T> reference = quantity.getUnit().getReferenceUnit();
            double valueLeft = quantity.to(reference).doubleValue();
            List<Quantity<T>> quantities = new ArrayList<Quantity<T>>(multiplicativeUnits.size());
            int i = 0;
            int limit = multiplicativeUnits.size();
            for (Unit<T> aUnit : multiplicativeUnits.values()) {
                double floor;
                if ((i + 1) < limit) {
                    floor = Math.floor(reference.of(valueLeft).to(aUnit).doubleValue());
                } else {
                    //floor = Math.rint(reference.of(valueLeft).to(aUnit).doubleValue());
                    floor = BigDecimal.valueOf(reference.of(valueLeft).to(aUnit).doubleValue())
                            .round(new MathContext(decPlaces + 1, RoundingMode.HALF_EVEN))
                            .doubleValue();
                }
                if (floor > 0.0) {
                    Quantity<T> floorUnit = aUnit.of(floor);
                    quantities.add(floorUnit);
                    valueLeft -= floorUnit.to(reference).doubleValue();
                }
                i++;
            }
            Quantity<T>[] qArr = new Quantity[quantities.size()];
            for (int j = 0; j < quantities.size(); j++) {
                qArr[j] = quantities.get(j);
            }
            return CompoundQuantity.of(qArr);
        } else {
            return null;
        }
    }

    /**
     * Instantiates a new Compound quantity.
     *
     * @param originalQuantity the original quantity
     * @param quantities       the quantities
     * @throws IllegalArgumentException if quantities contains less than 2 quantities
     */
    CompoundQuantity(Quantity<T> originalQuantity, @Nonnull Quantity<T>... quantities) throws IllegalArgumentException {
        this.originalQuantity = originalQuantity;
        if (quantities.length < 2) {
            throw new IllegalArgumentException("Compound quantity does not contain two or more quantities");
        }
        this.quantities = quantities;
    }

    /**
     * Gets separator.
     *
     * @return the separator
     */
    @Nonnull
    public String getSeparator() {
        return this.separator;
    }

    /**
     * Sets separator.
     *
     * @param separator the separator
     * @return the separator
     */
    @Nonnull
    public CompoundQuantity<T> setSeparator(String separator) {
        if (separator == null) {
            separator = "";
        }
        this.separator = separator;
        return this;
    }

    /**
     * Turn this compound quantity into a regular {@link Quantity}.
     *
     * @return the quantity
     */
    @Nonnull
    public Quantity<T> toQuantity() {
        if (this.originalQuantity != null) {
            return this.originalQuantity;
        }
        Quantity<T> q = quantities[0];
        for (int i = 1; i < quantities.length; i++) {
            q = q.add(quantities[i]);
        }
        return q;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(quantities.length * (4 + separator.length()));
        for (int i = 0; i < quantities.length; i++) {
            builder.append(quantities[i].toString());
            if ((i+1) < quantities.length) {
                builder.append(separator);
            }
        }
        return builder.toString();
    }
}
