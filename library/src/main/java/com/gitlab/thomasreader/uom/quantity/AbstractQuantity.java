/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.quantity.format.QuantityFormat;
import com.gitlab.thomasreader.uom.quantity.range.QuantityRange;
import com.gitlab.thomasreader.uom.unit.Unit;

import javax.annotation.Nonnull;

abstract class AbstractQuantity<T extends Quantity<T>> implements Quantity<T> {

    private final boolean isInterval;
    @Nonnull
    private final Unit<T> unit;

    AbstractQuantity(@Nonnull Unit<T> unit, boolean isInterval) {
        this.unit = unit;
        this.isInterval = isInterval;
    }

    @Override
    @Nonnull
    public Unit<T> getUnit() {
        return this.unit;
    }

    @Override
    public boolean isInterval() {
        return this.isInterval;
    }

    @Override
    @Nonnull
    public String toString() {
        return new QuantityFormat().format(this);
    }

    @Nonnull
    @Override
    public Quantity<T> min(@Nonnull Quantity<T> that) {
        return (this.compareTo(that) <= 0) ? this : that;
    }

    @Nonnull
    @Override
    public Quantity<T> max(@Nonnull Quantity<T> that) {
        return (this.compareTo(that) >= 0) ? this : that;
    }

    @Nonnull
    @Override
    public QuantityRange<T> plusOrMinus(@Nonnull Quantity<T> otherQuantity) {
        return this.subtract(otherQuantity).range(this.add(otherQuantity));
    }

    @Nonnull
    @Override
    public QuantityRange<T> range(@Nonnull Quantity<T> that) {

        return this.range(that, true, true);
    }

    @Nonnull
    @Override
    public QuantityRange<T> range(@Nonnull Quantity<T> that, boolean inclusiveStart, boolean inclusiveEnd) {
        //boolean bigPrecision = this.numberValue() instanceof BigDecimal;
        return new QuantityRange<T>(this, that, inclusiveStart, inclusiveEnd);
    }
}
