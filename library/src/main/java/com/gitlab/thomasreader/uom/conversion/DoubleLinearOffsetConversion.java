/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;


/**
 * Linear conversion using double for its coefficient and offset. The conversion resembles a
 * function of: {@code ax+c} or {@code (x+c)*a} where a is the coefficient and c is the constant.
 */
public class DoubleLinearOffsetConversion implements LinearConversion {
    private final double coefficient;
    private final double offset;
    private final boolean coefficientFirst;

    DoubleLinearOffsetConversion(double coefficient, double offset, boolean coefficientFirst) {
        this.coefficient = coefficient;
        this.offset = offset;
        this.coefficientFirst = coefficientFirst;
    }

    /**
     * Creates a linear conversion of the form {@code ax+c}.
     *
     * @param coefficient the coefficient
     * @param offset      the offset
     * @return the linear conversion
     */
    @Nonnull
    public static DoubleLinearOffsetConversion coefficientThenOffset(
            double coefficient, double offset
    ) {
        return new DoubleLinearOffsetConversion(coefficient, offset, true);
    }

    /**
     * Creates a linear conversion of the form {@code (x+c)*a}.
     *
     * @param offset      the offset
     * @param coefficient the coefficient
     * @return the linear conversion
     */
    @Nonnull
    public static DoubleLinearOffsetConversion offsetThenCoefficient(
            double offset, double coefficient
    ) {
        return new DoubleLinearOffsetConversion(coefficient, offset, false);
    }

    @Override
    public double getCoefficient() {
        return this.coefficient;
    }

    @Nonnull
    @Override
    public BigDecRational getRationalCoefficient() {
        return BigDecRational.valueOf(this.getCoefficient());
    }

    @Override
    public double convert(double input) {
        return (coefficientFirst)?
                (input * this.coefficient) + this.offset : (input + this.offset) * this.coefficient;
    }

    @Override
    public double inverse(double input) {
        return (coefficientFirst)?
                (input - offset) / this.coefficient : (input / coefficient) - this.offset;
    }

    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        if (coefficientFirst) {
            return input
                    .multiply(BigDecimal.valueOf(this.coefficient), mathContext)
                    .add(BigDecimal.valueOf(this.offset), mathContext);
        } else {
            return input
                    .add(BigDecimal.valueOf(this.offset), mathContext)
                    .multiply(BigDecimal.valueOf(this.coefficient), mathContext);
        }
    }

    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        if (coefficientFirst) {
            return input
                    .subtract(BigDecimal.valueOf(this.offset), mathContext)
                    .divide(BigDecimal.valueOf(this.coefficient), mathContext);
        } else {
            return input
                    .divide(BigDecimal.valueOf(this.coefficient), mathContext)
                    .subtract(BigDecimal.valueOf(this.offset), mathContext);
        }
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder(10);
        if (coefficientFirst) {
            string.append(this.coefficient)
                    .append("x");
            if (this.offset < 0) {
                string.append(this.offset);
            } else if (this.offset > 0) {
                string.append("+")
                        .append(this.offset);
            }
        } else {
            if (this.offset != 0) {
                string.append("(x");
                if (this.offset < 0) {
                    string.append(this.offset);
                } else if (this.offset > 0) {
                    string.append("+")
                            .append(this.offset);
                }
                string.append(")*")
                        .append(this.coefficient);
            } else {
                string.append(this.coefficient)
                        .append("x");
            }
        }
        return string.toString();
    }
}
