/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.math;

import java.math.MathContext;
import java.math.RoundingMode;

import javax.annotation.Nonnull;

/**
 * Useful mathematics using doubles
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class DoubleMath {
    /**
     * Rounds a double to 0 decimal places using the rounding mode. {@link RoundingMode#UNNECESSARY}
     * is treated as if it was {@link RoundingMode#HALF_EVEN}.
     *
     * @param d            the double to be rounded
     * @param roundingMode the rounding mode
     * @return the rounded double
     */
    public static double round(double d, @Nonnull RoundingMode roundingMode) {
        double multi = (d < 0) ? -1 : 1;
        switch (roundingMode) {
            case CEILING:
                return Math.ceil(d);
            case FLOOR:
                return Math.floor(d);
            case UP:
                return Math.ceil(Math.abs(d)) * multi;
            case DOWN:
                return Math.floor(Math.abs(d)) * multi;
            case HALF_UP:
                return Math.floor(Math.abs(d) + 0.5) * multi;
            case HALF_DOWN:
                return Math.ceil(Math.abs(d) - 0.5) * multi;
            case UNNECESSARY:
            case HALF_EVEN:
            default:
                return Math.rint(d);
        }
    }

    /**
     * Sets the precision of a double. Equivalent to {@link java.math.BigDecimal#round(MathContext)}
     * but performs the operation in ~30% the time. Some precision is lost in ~20% of cases.
     *
     * @param d           the double
     * @param mathContext the math context specifying the precision to use
     * @return the double rounded according to mathContext
     */
    public static double setPrecision(double d, MathContext mathContext) {
        // Cap sig digits to 17 (max precision for double)
        int sigDigits = Math.min(17, mathContext.getPrecision());
        // Get log of double, number of digits before decimal place = ceil(log10(abs(d))))
        // 123.456 = 10^2.09... -> ceil = 3 digits
        // if we want a precision of 4 then -> 3 - 4 = -1. This is power for shifting the
        // decimal places so rounding can effectively take place.
        // In this case 123.456 * 10^1 = 1234.56
        int log10 = (int) Math.ceil(Math.log10(Math.abs(d)));
        int power = log10 - sigDigits;
        double result;
        // separate negative powers and positive powers for more effective double precision as
        // a positive power is more likely to represented by a double and y * 10^x == y / 10^-x
        if (power < 0) {
            double tenPower = Math.pow(10, Math.abs(power));
            result = d * tenPower;
            result = round(result, mathContext.getRoundingMode());
            result = result / tenPower;
        }
//            result = BigDecimal.valueOf(d).round(mathContext).doubleValue();
//        } else if (power > 22) {
//            result = BigDecimal.valueOf(d).round(mathContext).doubleValue();
//        }
        else {
            double tenPower = Math.pow(10, power);
            result = d / tenPower;
            result = round(result, mathContext.getRoundingMode());
            result = result * tenPower;
        }
        return result;
    }
}
