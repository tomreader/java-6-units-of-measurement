/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit.prefix;

import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.Unit;

import javax.annotation.Nonnull;

/**
 * <p>
 * Interface to model a <a href="https://en.wikipedia.org/wiki/Unit_prefix">Unit prefix</a> by
 * prepending a prefix to a {@link Unit} of measurement.
 * </p>
 * <p>
 * The prefixes of the metric system, such as kilo and milli, represent multiplication by
 * powers of ten. In information technology it is common to use binary prefixes,
 * which are based on powers of two.
 * </p>
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Unit_prefix">Wikipedia - Unit prefix</a>
 * @since 0.1.0
 */
public interface UnitPrefix {

    /**
     * Gets the symbol of the prefix..
     *
     * @return the symbol
     */
    @Nonnull
    public String getSymbol();
    
    /**
     * Gets the conversion to revert back to the {@link Unit}.
     *
     * @return the conversion
     */
    @Nonnull
    public MultiplicativeConversion getConversion();
    
    /**
     * Prepend the {@code unit} with {@code this} prefix. If the {@code unit} cannot be prefixed
     * then null is returned.
     *
     * @param <T>
     *         the type of quantity
     * @param unit
     *         the unit to be prefixed
     * @return the prefixed unit or null if unit cannot be prefixed
     * @see Unit#canPrefix()
     */
    public <T extends Quantity<T>> Unit<T> prefix(@Nonnull Unit<T> unit);

    /**
     * Prepend the {@code unit} with {@code this} prefix. If the {@code unit} cannot be prefixed
     * then an exception is thrown.
     *
     * @param <T>
     *         the type of quantity
     * @param unit
     *         the unit to be prefixed
     * @return the prefixed unit
     * @throws IllegalArgumentException if the unit cannot be prefixed
     * @see Unit#canPrefix()
     */
    @Nonnull
    public <T extends Quantity<T>> Unit<T> prefixOrThrow(@Nonnull Unit<T> unit) throws IllegalArgumentException;

}
