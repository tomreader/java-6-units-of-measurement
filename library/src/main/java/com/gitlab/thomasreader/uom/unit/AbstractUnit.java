/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.quantity.DecimalQuantity;
import com.gitlab.thomasreader.uom.quantity.DoubleQuantity;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;


import java.math.BigDecimal;

import javax.annotation.Nonnull;

/**
 * Implementation of common methods of {@link Unit<T>}.
 *
 * @param <T>
 *         the type of quantity
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class AbstractUnit<T extends Quantity<T>> implements Unit<T> {

    private final boolean canPrefix;
    private UnitSymbolFormat symbolFormat;

    public AbstractUnit(boolean canPrefix, UnitSymbolFormat symbolFormat) {
        this.canPrefix = canPrefix;
        this.symbolFormat = (symbolFormat == null) ? UnitSymbolFormat.DEFAULT : symbolFormat;
    }

    @Override
    public boolean canPrefix() {
        return this.canPrefix;
    }

    @Nonnull
    @Override
    public UnitSymbolFormat getSymbolFormat() {
        return (symbolFormat == null) ? UnitSymbolFormat.DEFAULT : this.symbolFormat;
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> of(double value) {
        return new DoubleQuantity<T>(value, this);
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> of(BigDecimal value) {
        return new DecimalQuantity<T>(value, this);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder()
                .append("Unit of ")
                .append(this.getType().getSimpleName())
                .append(": ")
                .append(this.getSymbol());
        if ((this instanceof ReferenceUnit)) {
            stringBuilder.append(" (reference unit)");
        } else {
            if (this.getConversion() == Conversions.identity()) {
                stringBuilder
                        .append(", alias for ")
                        .append(this.getReferenceUnit().getSymbol());
            } else {
                stringBuilder
                        .append(", conversion to ")
                        .append(this.getReferenceUnit().getSymbol())
                        .append(": ")
                        .append(this.getConversion());
            }
        }
         return stringBuilder.toString();
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object o);
}
