/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.prefix.MetricPrefix;
import com.gitlab.thomasreader.uom.unit.prefix.UnitPrefix;


import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * Models a unit which is composed of both a {@link UnitPrefix} and a {@link Unit<T>}.
 * A kilogram is composed of
 * {@link MetricPrefix#KILO KILO} and
 * a {@code Unit<T>} modelling a gram.
 *
 * @param <T>
 *         the quantity type
 * @version 0.1.0
 * @since 0.1.0
 */
public class PrefixedUnit<T extends Quantity<T>> extends AbstractUnit<T> implements Unit<T> {
    @Nonnull
    private final UnitPrefix prefix;
    @Nonnull
    private final Unit<T> prefixedUnit;

    public PrefixedUnit(@Nonnull UnitPrefix prefix, @Nonnull Unit<T> prefixedUnit) {
        super(false, prefixedUnit.getSymbolFormat());
        this.prefix = prefix;
        this.prefixedUnit = prefixedUnit;
    }

    @Nonnull
    public UnitPrefix getPrefix() {
        return this.prefix;
    }
    
    /**
     * Returns the unit that is prefixed.
     *
     * @return the prefixed unit
     */
    @Nonnull
    public Unit<T> getPrefixedUnit() {
        return this.prefixedUnit;
    }

    @Nonnull
    @Override
    public UnitConversion getConversion() {
        if (prefixedUnit.getConversion() instanceof MultiplicativeConversion) {
            return Conversions.foldDouble(this.prefix.getConversion(), (MultiplicativeConversion) prefixedUnit.getConversion());
        }
        return Conversions.of(
                this.getPrefix().getConversion(), this.getPrefixedUnit().getConversion()
        );
    }
    
    /**
     * Returns the symbol of the prefix and the parent unit.
     *
     * @return prefix's symbol appended by the parent unit's symbol
     */
    @Nonnull
    @Override
    public String getSymbol() {
        return this.getPrefix().getSymbol() + this.getPrefixedUnit().getSymbol();
    }

    @Nonnull
    @Override
    public Class<T> getType() {
        return this.getPrefixedUnit().getType();
    }

    @Nonnull
    @Override
    public Unit<T> getReferenceUnit() {
        return this.getPrefixedUnit().getReferenceUnit();
    }

    @Nonnull
    @Override
    public String toString() {
        return this.getSymbol();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrefixedUnit<?> that = (PrefixedUnit<?>) o;
        return getPrefix().equals(that.getPrefix()) &&
                getPrefixedUnit().equals(that.getPrefixedUnit());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrefix(), getPrefixedUnit());
    }
}
