/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.quantity.format.QuantityFormat;
import com.gitlab.thomasreader.uom.quantity.range.QuantityRange;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.quantity.type.Mass;
import com.gitlab.thomasreader.uom.unit.prefix.MetricPrefix;
import com.gitlab.thomasreader.uom.math.FunctionXY;
import com.gitlab.thomasreader.uom.unit.Unit;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;

import javax.annotation.Nonnull;

/**
 * <p>
 * Resembles a <a href="https://en.wikipedia.org/wiki/Quantity">quantity</a> which by assigning
 * a magnitude in terms of a {@link Unit <T>} of measurement.
 * </p>
 * <p>
 * Quantities can be compared in terms of "more", "less", or "equal"
 * and if a quantity is comparable then it can be operated on.
 * </p>
 *
 * @param <T>
 *         the type of physical quantity: common are
 *         {@linkplain Mass Mass} and
 *         {@linkplain Length Length}
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Quantity">Wikipedia - Quantity</a>
 * @see <a href="http://www.dsc.ufcg.edu.br/~jacques/cursos/map/recursos/fowler-ap/Analysis%20Pattern%20Quantity.htm">
 *     Martin Fowler - Quantity</a>
 * @since   0.1.0
 */
public interface Quantity<T extends Quantity<T>> extends Comparable<Quantity<T>> {

    /**
     * Gets the {@code Number} value of this.
     *
     * @return the value
     */
    @Nonnull
    public Number numberValue();

    /**
     * Gets the {@code double} value of this.
     *
     * @return the value
     */
    public double doubleValue();

    /**
     * Gets the {@code BigDecimal} value of this.
     *
     * @return the value
     */
    @Nonnull
    public BigDecimal bigDecimalValue();

    /**
     * Gets magnitude of this as formatted by the numberFormatter.
     *
     * @param numberFormatter
     *         the number formatter
     * @return the formatted magnitude
     * @see NumberFormat
     */
    @Nonnull
    public String getValue(@Nonnull NumberFormat numberFormatter);


    /**
     * Indicates whether this resembles an interval or not primarily useful for quantities such as
     * temperature: where 50 &deg;C as an interval is also a 50 K interval;
     * but a measurement of 50 &deg;C is 323.15 K.
     *
     * @return whether this resembles an interval
     * @see <a href="https://en.wikipedia.org/wiki/Level_of_measurement#Interval_scale">
     *     Wikipedia: Level of measurement - Interval scale</a>
     * @see <a href="https://www.bipm.org/en/CGPM/db/13/3/">
     *     BIPM: Resolution 3 of the 13th CGPM (1967)</a>
     */
    public boolean isInterval();

    /**
     * Gets the unit of this.
     *
     * @return the {@code Unit<T>}
     */
    @Nonnull
    public Unit<T> getUnit();

    /**
     * Returns a {@code Quantity<T>} whose value is converted to the target unit.
     *
     * @param target
     *         the unit to convert to
     * @return the {@code Quantity<T>} with the supplied {@code target}
     */
    @Nonnull
    public Quantity<T> to(@Nonnull Unit<T> target);

    /**
     * Returns a quantity whose new value is equal to this in its root unit
     * multiplied by the {@code multiplicand}.
     *
     * The result for common multiplicative units such as a kilogram will be {@code 3 kg * 2 = 6 kg}.
     *
     * If the unit is linear but not multiplicative i.e. Celsius then if this quantity is an
     * interval a conversion as above will take place. If it is not an interval or
     * when the unit is also not linear then the value will first be converted back to the
     * root unit value; then the multiplication will take place.
     *
     * <b>Examples:</b>>
     * A measurement of 0 Celsius in Kelvin is 273.15: {@code 273.15 * 2 = 476.3} then converting
     * back to Celsius will be 273.15 Celsius - not 0.
     *
     * A measurement of 20 decibels as a dimensionless number is 100: {@code 100 * 2 = 200} then
     * converting back to decibels is ~23 decibels.
     *
     * @param multiplicand
     *         the value to multiply {@code this} by
     * @return {@code this * multiplicand}
     */
    @Nonnull
    public Quantity<T> multiply(double multiplicand);

    /**
     * Returns a quantity whose new value is equal to this in its root unit
     * multiplied by the {@code multiplicand}.
     *
     * The result for common multiplicative units such as a kilogram will be {@code 3 kg * 2 = 6 kg}.
     *
     * If the unit is linear but not multiplicative i.e. Celsius then if this quantity is an
     * interval a conversion as above will take place. If it is not an interval or
     * when the unit is also not linear then the value will first be converted back to the
     * root unit value; then the multiplication will take place.
     *
     * <b>Examples:</b>>
     * A measurement of 0 Celsius in Kelvin is 273.15: {@code 273.15 * 2 = 476.3} then converting
     * back to Celsius will be 273.15 Celsius - not 0.
     *
     * A measurement of 20 decibels as a dimensionless number is 100: {@code 100 * 2 = 200} then
     * converting back to decibels is ~23 decibels.
     *
     * @param multiplicand
     *         the value to multiply {@code this} by
     * @return {@code this * multiplicand}
     */
    @Nonnull
    public Quantity<T> multiply(@Nonnull BigDecimal multiplicand);

    /**
     * Returns a quantity whose new value is equal to this in its root unit
     * divided by the {@code dividend}.
     *
     * The result for common multiplicative units such as a kilogram will be {@code 3 kg / 2 = 1.5 kg}.
     *
     * If the unit is linear but not multiplicative i.e. Celsius then if this quantity is an
     * interval a conversion as above will take place. If it is not an interval or
     * when the unit is also not linear then the value will first be converted back to the
     * root unit value; then the division will take place.
     *
     * <b>Examples:</b>>
     * A measurement of 0 Celsius in Kelvin is 273.15: {@code 273.15 / 2 = 136.575} then converting
     * back to Celsius will be −136.575 Celsius - not 0.
     *
     * A measurement of 20 decibels as a dimensionless number is 100: {@code 100 / 2 = 50} then
     * converting back to decibels is ~17 decibels.
     *
     * @param dividend
     *         the value to divide {@code this} by.
     * @return {@code this / multiplicand}
     * @throws ArithmeticException if {@code divisor == 0}
     */
    @Nonnull
    public Quantity<T> divide(double dividend) throws ArithmeticException;

    /**
     * Returns a quantity whose new value is equal to this in its root unit
     * divided by the {@code dividend}.
     *
     * The result for common multiplicative units such as a kilogram will be {@code 3 kg / 2 = 1.5 kg}.
     *
     * If the unit is linear but not multiplicative i.e. Celsius then if this quantity is an
     * interval a conversion as above will take place. If it is not an interval or
     * when the unit is also not linear then the value will first be converted back to the
     * root unit value; then the division will take place.
     *
     * <b>Examples:</b>>
     * A measurement of 0 Celsius in Kelvin is 273.15: {@code 273.15 / 2 = 136.575} then converting
     * back to Celsius will be −136.575 Celsius - not 0.
     *
     * A measurement of 20 decibels as a dimensionless number is 100: {@code 100 / 2 = 50} then
     * converting back to decibels is ~17 decibels.
     *
     * @param dividend
     *         the value to divide {@code this} by
     * @return {@code this / multiplicand}
     * @throws ArithmeticException if {@code dividend == 0}
     */
    @Nonnull
    public Quantity<T> divide(@Nonnull BigDecimal dividend) throws ArithmeticException;


    /**
     * Returns a dimensionless ratio of {@code this / dividend}
     *
     * <b>Examples:</b>
     * {@code 50 kg / 25 kg = 2}
     * {@code 50 K / 25 K = 2}
     * {@code 50 &deg;C / 25 &deg;C = 323.15 K / 298.15 K =~ 1.08}
     * {@code 50 dB / 25 dB = 10000 / ~316 =~ 31.6}
     *
     *
     *
     * @param dividend the quantity to divide this by
     * @return a dimensionless ratio between {@code this} and {@code dividend}
     * @throws ArithmeticException if {@code dividend == 0 in its root unit}
     */
    @Nonnull
    public Quantity<Dimensionless> divide(@Nonnull Quantity<T> dividend) throws ArithmeticException;

    /**
     * Returns a quantity whose new value is equal to this in its root unit
     * plus the {@code augend}.
     *
     * The result for common multiplicative units such as a kilogram will be {@code 3 kg + 2 kg = 5 kg}.
     *
     * If either of the quantities are an interval and are not multiplicative units then the
     * result will be {@code 0 &deg;C + Δ10 &deg;C = 10 &deg;C }
     *
     * If it is not an interval or when the unit is also not linear then the value will
     * first be converted back to the root unit value; then the addition will take place.
     *
     * <b>Examples:</b>>
     * Adding two measurements of 0 Celsius in Kelvin are 273.15: {@code 273.15 + 273.15 = 546.3}
     * then converting back to Celsius will be 273.15 Celsius not 0 - however adding two
     * temperature measurements together is meaningless.
     *
     * A measurement of 20 decibels as a dimensionless number is 100, adding two together:
     * {@code 100 + 100 = 200} then converting back to decibels is ~23 decibels.
     *
     * @param augend
     *         the quantity to be added to this {@code Quantity<T>}
     * @return {@code this + augend}
     */
    @Nonnull
    public Quantity<T> add(@Nonnull Quantity<T> augend);

    /**
     * Returns a quantity whose new value is equal to this in its root unit
     * minus the {@code subtrahend}.
     *
     * The result for common multiplicative units such as a kilogram will be {@code 3 kg - 2 kg = 1 kg}.
     *
     * If either of the quantities are an interval and are not multiplicative units then the
     * result will be {@code 0 &deg;C - Δ10 &deg;C = -10 &deg;C }
     *
     * If it is not an interval or when the unit is also not linear then the value will first be
     * converted back to the root unit value; then the subtraction will take place.
     *
     * <b>Examples:</b>>
     * Subtracting two measurements of 0 Celsius in Kelvin are 273.15: {@code 273.15 - 273.15 = 0}
     * then converting back to Celsius will be -273.15 Celsius - not 0.
     *
     * A measurement of 20 decibels as a dimensionless number is 100,
     * subtracting 19 decibels (~ 79.4 dimensionless number) from it:
     * {@code 100 - 79.4 = ~20.6} then converting back to decibels is ~13.1 decibels.
     *
     * Note: Subtracting 20 decibels from 20 decibels would produce a dimensionless number of 0.
     * This is then not able to be converted back to decibels as 10log(0) is undefined.
     *
     * @param subtrahend
     *         the quantity to be added to this {@code Quantity<T>}
     * @return {@code this - subtrahend}
     */
    @Nonnull
    public Quantity<T> subtract(@Nonnull Quantity<T> subtrahend);

    /**
     * This is the same as {@link #subtract(Quantity)} but produces a quantity which is
     * an interval.
     *
     *
     * @param subtrahend
     *         the quantity to be added to this {@code Quantity<T>}
     * @return an interval quantity of value {@code this - subtrahend}
     */
    @Nonnull
    public Quantity<T> interval(@Nonnull Quantity<T> subtrahend);

    /**
     * Simply returns a quantity whose value is {@code this.numberValue() + 1}
     *
     * Note: this is not the same for some units as adding 1 of this unit.
     *
     * @return {@code this + 1}
     */
    @Nonnull
    public Quantity<T> inc();

    /**
     * Simply returns a quantity whose value is {@code this.numberValue() - 1}
     *
     * Note: this is not the same for some units as subtracting 1 of this unit.
     *
     * @return {@code this - 1}
     */
    @Nonnull
    public Quantity<T> dec();

    /**
     * Returns a {@code Quantity<T>} whose value is {@code this.numberValue() * -1}.
     *
     * @return {@code this * -1}
     */
    @Nonnull
    public Quantity<T> negate();

    /**
     * Creates a {@link QuantityRange <T>} from {@code this} (inclusive)
     * and {@code that} (inclusive)
     *
     * @param that
     *         the other quantity
     * @return the quantity range from {@code this} to {@code that}
     * @see QuantityRange
     * @see Quantity#range(Quantity, boolean, boolean)
     */
    @Nonnull
    public QuantityRange<T> range(@Nonnull Quantity<T> that);

    /**
     * Range to quantity range.
     *
     * @param that
     *         the other quantity
     * @param inclusiveStart
     *         whether the start is inclusive ({@code this})
     * @param inclusiveEnd
     *         whether the end is inclusive ({@code that})
     * @return the quantity range from {@code this} to {@code that}
     * @see QuantityRange
     */
    @Nonnull
    public QuantityRange<T> range(@Nonnull Quantity<T> that, boolean inclusiveStart, boolean inclusiveEnd);

    /**
     * Create a {@link QuantityRange <T>} from {@code this - delta} to
     * {@code this + delta}
     * Example:
     * {@code 5 metres plus or minus 2 metres = 3..7 metres}
     *
     * @param delta
     *         the delta
     * @return the quantity range
     * @see QuantityRange
     * @see <a href="https://en.wiktionary.org/wiki/plus-minus_sign">
     *     Wiktionary - plus-minus sign</a>
     */
    @Nonnull
    public QuantityRange<T> plusOrMinus(@Nonnull Quantity<T> delta);

    /**
     * Compares two quantities to provide natural ordering.
     * @param otherQuantity the {@code Quantity} to be compared
     * @return the value {@code 0} if the quantities are equal,
     * a value less than 0 if {@code otherQuantity} is greater than {@code this},
     * or a value greater than 0 if {@code otherQuantity} is less than {@code this}
     */
    @Override
    public int compareTo(@Nonnull Quantity<T> otherQuantity);


    /**
     * Returns the string representation of this {@code Quantity} using the default formatter.
     *
     * @return the string representation of this {@code Quantity}
     * @see QuantityFormat
     */
    @Nonnull
    public String toString();
    
    /**
     * Returns a quantity with the same unit and a positive value
     *
     * @return this with a positive value
     */
    @Nonnull
    public Quantity<T> abs();

    /**
     * Returns a {@code Quantity<T>} whose values precision matches that of the {@code MathContext}.
     * If this values precision is greater than MathContext then rounding will occur.
     *
     * @param mathContext
     *         the math context
     * @return the rounded {@code Quantity<T>}
     * @see MathContext
     */
    @Nonnull
    public Quantity<T> setPrecision(@Nonnull MathContext mathContext);

    /**
     * Compares two quantities to provide natural ordering with an epsilon (tolerance). This epsilon
     * is scaled to the sizes of the quantities involved.
     *
     * @param that
     *      the {@code Quantity} to be compared
     * @param epsilon
     *      the tolerance factor
     * @return the value {@code 0} if the quantities are equal or the difference is smaller than
     * the {@code epsilon}, a value less than 0 if {@code otherQuantity} is greater than
     * {@code this}, or a value greater than 0 if {@code otherQuantity} is less than {@code this}
     *
     * @see <a href="https://en.wikipedia.org/wiki/Floating-point_arithmetic#Accuracy_problems">
     *     Wikipedia: Floating-point arithment - Accuracy problems</a>
     * @see <a href="http://c-faq.com/fp/fpequal.html">
     *     What's a good way to check for ``close enough'' floating-point equality? </a>
     */
    public int compareTo(@Nonnull Quantity<T> that, double epsilon);

    /**
     * In any quantitative science, the terms relative change and relative difference are
     * used to compare two quantities while taking into account the "sizes" of the things
     * being compared. The comparison is expressed as a ratio and is a unitless number.
     *
     * By multiplying these ratios by 100 they can be expressed as percentages so the terms
     * percentage change, percent(age) difference, or relative percentage difference are also
     * commonly used.
     *
     * The relative change between this and the reference value is defined as:
     * {@code (this - reference) / abs(reference)}
     *
     * Note: Combining this with abs() will perform a absolute relative change.
     *
     * @param reference the reference value to create a relative change from
     * @return {@code (this - reference) / abs(reference)}
     * @throws ArithmeticException if {@code reference == 0}
     * @see <a href="https://en.wikipedia.org/wiki/Relative_change_and_difference#Definitions">
     *     Wikipedia: Relative change and difference</a>
     */
    @Nonnull
    public Quantity<Dimensionless> relativeChange(@Nonnull Quantity<T> reference) throws ArithmeticException;

    /**
     * In any quantitative science, the terms relative change and relative difference are
     * used to compare two quantities while taking into account the "sizes" of the things
     * being compared. The comparison is expressed as a ratio and is a unitless number.
     *
     * By multiplying these ratios by 100 they can be expressed as percentages so the terms
     * percentage change, percent(age) difference, or relative percentage difference are also
     * commonly used.
     *
     * If the relationship of the value with respect to the reference value
     * (that is, larger or smaller) does not matter in a particular application,
     * the absolute difference may be used in place of the actual change in the above formula
     * to produce a value for the relative change which is always non-negative.
     *
     * @param that the quantity to compare with
     * @param function the function which takes the values of {@code this} and {@code that}
     * @return {@code (abs(this-that)) / abs(function(this, that))}
     * @throws ArithmeticException if the {@code function} returns a value of 0
     * @see <a href="https://en.wikipedia.org/wiki/Relative_change_and_difference#Definitions">
     *     Wikipedia: Relative change and difference</a>
     */
    @Nonnull
    public Quantity<Dimensionless> relativeDifference(
            @Nonnull Quantity<T> that, @Nonnull FunctionXY function) throws ArithmeticException;

    /**
     * Returns the smallest of {@code this} or {@code that}.
     *
     * @param that the quantity to compare with
     * @return the smallest quantity between {@code this} and {@code that}
     */
    @Nonnull
    public Quantity<T> min(@Nonnull Quantity<T> that);

    /**
     * Returns the largest of {@code this} or {@code that}.
     *
     * @param that the quantity to compare with
     * @return the largest quantity between {@code this} and {@code that}
     */
    @Nonnull
    public Quantity<T> max(@Nonnull Quantity<T> that);

    /**
     * Converts this quantity into a more human readable unit using {@link MetricPrefix}
     * providing {@link Quantity#getUnit()} can be prefixed, if not this is returned.
     *
     * @return the quantity
     */
    @Nonnull
    public Quantity<T> toHuman();
//      public <U extends Quantity<U>> Quantity<U> reciprocal(Unit<U> to);
//    /**
//     * Helper method to multiply {@code this} by another {@code multiplicandQuantity} to
//     * create a {@code UnitQuantityCreator} useful for converting within types.
//     *
//     * Example:
//     * {@code METRE.of(10).times(SQUARE_METRE.of(25)).as(CUBIC_METRE)}
//     *
//     * @param multiplicandQuantity
//     *         the multiplicand quantity
//     * @return the {@code UnitQuantityCreator} whose value is
//     * {@code this.getMagnitude() * multiplicandQuantity.getMagnitude()}
//     * @see <a href="https://en.wikipedia.org/wiki/SI_derived_unit">
//     *     Wikipedia - SI derived unit</a>
//     */
//    @Nonnull
//    public <U extends Quantity<U>> Quantity<U> times(@Nonnull Quantity<?> multiplicandQuantity, @Nonnull Unit<U> newUnit);
//
//    /**
//     * Helper method to divide {@code this} by another {@code divisorQuantity} to
//     * create a {@code UnitQuantityCreator} useful for converting within types.
//     *
//     * @param divisorQuantity
//     *         the divisorQuantity quantity
//     * @return the {@code UnitQuantityCreator} whose value is
//     * {@code this.getMagnitude() / multiplicandQuantity.getMagnitude()}
//     * @throws ArithmeticException if {@code divisorQuantity.getMagnitude() == 0}
//     */
//    @Nonnull
//    public <U extends Quantity<U>> Quantity<U> div(@Nonnull Quantity<?> divisorQuantity, @Nonnull Unit<U> newUnit) throws ArithmeticException;
}
