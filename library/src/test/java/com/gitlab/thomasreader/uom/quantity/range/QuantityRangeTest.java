/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity.range;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.quantity.DecimalQuantity;
import com.gitlab.thomasreader.uom.quantity.DoubleQuantity;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.UnitBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class QuantityRangeTest {

    static Unit<Length> METRE;
    static Unit<Length> INCH;
    static Unit<Length> MILE;
    static Unit<Length> INCH_DECIMAL;
    static Unit<Length> MILE_DECIMAL;

    @Before
    public void setUp() throws Exception {
        METRE = UnitBuilder.symbol("m").referenceUnit(Length.class);
        INCH = UnitBuilder.symbol("in").unit(METRE, Conversions.multiply(0.0254));
        MILE = UnitBuilder.symbol("mi").unit(METRE, Conversions.multiply(1609.344));
        INCH_DECIMAL =
                UnitBuilder.symbol("in").unit(METRE, Conversions.multiply(new BigDecimal("0.0254")));
        MILE_DECIMAL =
                UnitBuilder.symbol("mi").unit(METRE, Conversions.multiply(new BigDecimal("1609.344")));
    }

    @After
    public void tearDown() throws Exception {
        METRE = null;
        INCH = null;
        MILE = null;
        INCH_DECIMAL = null;
        MILE_DECIMAL = null;
    }

    @Test
    public void contains() {
        DoubleQuantity<Length> metres5 = METRE.of(5);
        DoubleQuantity<Length> metres0 = METRE.of(0);
        DoubleQuantity<Length> metres10 = METRE.of(10);
        assertEquals(true, new QuantityRange<Length>(metres0, metres10).contains(metres5));
        assertEquals(true, new QuantityRange<Length>(metres0, metres5).contains(metres5));
        assertEquals(false, new QuantityRange<Length>(metres0, metres5,false, false).contains(metres5));
        assertEquals(false, new QuantityRange<Length>(metres0, metres5,false, false).contains(metres0));

        DecimalQuantity<Length> inches63360 = INCH_DECIMAL.of(BigDecimal.valueOf(63360));
        DecimalQuantity<Length> mile1 = MILE_DECIMAL.of(BigDecimal.ONE);
        assertEquals(true, new QuantityRange<Length>(inches63360, mile1).contains(inches63360));
        assertEquals(true, new QuantityRange<Length>(inches63360, mile1).contains(mile1));
        assertEquals(false, new QuantityRange<Length>(inches63360, mile1,false, false).contains(inches63360));
        assertEquals(false, new QuantityRange<Length>(inches63360, mile1,false, false).contains(mile1));

        // check for infinity
        assertEquals(true, new QuantityRange<Length>(metres0, null).contains(metres5));
        assertEquals(true, new QuantityRange<Length>(null, metres5).contains(metres0));
        assertEquals(true, new QuantityRange<Length>(null, null).contains(METRE.of(Double.POSITIVE_INFINITY)));
    }
}