/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.junit.Assert.*;

public class DoubleOffsetConversionTest {

    static OffsetConversion.DoubleOffset celsius =
            new OffsetConversion.DoubleOffset(273.15);

    static double delta = 0.000001;

    @Test
    public void convert() {
        assertEquals(273.15, celsius.convert(0), delta);
        assertEquals(310.15, celsius.convert(37), delta);
    }

    @Test
    public void inverse() {
        assertEquals(273.15, celsius.inverse(546.3), delta);
        assertEquals(310.15, celsius.inverse(583.3), delta);
    }

    @Test
    public void convertBigDecimal() {
        assertEquals(new BigDecimal("273.15"),
                celsius.convert(BigDecimal.ZERO, MathContext.DECIMAL128));
        assertEquals(new BigDecimal("310.15"),
                celsius.convert(BigDecimal.valueOf(37), MathContext.DECIMAL128));
    }

    @Test
    public void inverseBigDecimal() {
        assertEquals(new BigDecimal("273.15"),
                celsius.inverse(new BigDecimal("546.3"), MathContext.DECIMAL128));
        assertEquals(new BigDecimal("310.15"),
                celsius.inverse(new BigDecimal("583.3"), MathContext.DECIMAL128));
    }
}