/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class SuperscriptFormatTest {

    static String ZERO = "\u2070";
    static String ONE = "\u00B9";
    static String TWO = "\u00B2";
    static String THREE = "\u00B3";
    static String FOUR = "\u2074";
    static String FIVE = "\u2075";
    static String SIX = "\u2076";
    static String SEVEN = "\u2077";
    static String EIGHT = "\u2078";
    static String NINE = "\u2079";
    static String NEGATIVE = "\u207b";

    @Test
    public void fromInt() {
        this.fromIntHelper(ONE + TWO + THREE, 123);
        this.fromIntHelper(NINE + EIGHT + SEVEN + SIX + FIVE, 98765);
        this.fromIntHelper(FOUR + THREE + TWO + ONE + ZERO, 43210);
        this.fromIntHelper(NEGATIVE + ONE + THREE + FIVE + SEVEN + NINE, -13579);
    }

    private void fromIntHelper(String expectedValue, int input) {
        String result = SuperscriptFormat.fromInt(input);
        assertEquals(
                "Expected value was: " + expectedValue + ", value got was: " + result,
                expectedValue,
                result
         );
    }
}