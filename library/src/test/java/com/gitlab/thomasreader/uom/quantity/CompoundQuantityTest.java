/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.UnitBuilder;

import org.junit.Test;

import static org.junit.Assert.*;

public class CompoundQuantityTest {


    @Test
    public void toQuantity() {
        Unit<Length> metre = UnitBuilder.symbol("m").referenceUnit(Length.class);
        Unit<Length> inch = UnitBuilder.symbol("in").unit(metre, Conversions.multiply(0.0254));
        Unit<Length> feet = UnitBuilder.symbol("ft").unit(metre, Conversions.multiply(0.3048));
        CompoundQuantity<Length> c = CompoundQuantity.<Length>of(
                feet.of(4), inch.of(6)
        );
        CompoundQuantity<Length> c2 = CompoundQuantity.of(metre.of(1.3716), 0, feet, inch);
        assertEquals(4.5, c.toQuantity().doubleValue(), 0.0000001);
        assertEquals(4.5, c2.toQuantity().to(feet).doubleValue(), 0.0000001);
    }
}