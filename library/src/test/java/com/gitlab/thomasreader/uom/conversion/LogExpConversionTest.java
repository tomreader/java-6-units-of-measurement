/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogExpConversionTest {

    static LogExpConversion bel;
    static LogExpConversion neper;
    static double delta = 0.000001;

    @Before
    public void setUp() throws Exception {
        bel = LogExpConversion.exp(10);
        neper = LogExpConversion.exp(Math.E);
    }

    @After
    public void tearDown() throws Exception {
        bel = null;
        neper = null;
    }

    @Test
    public void convert() {
        assertEquals(1, bel.convert(0), delta);
        assertEquals(10, bel.convert(1), delta);
        assertEquals(0.7943282347242815, bel.convert(-0.1), delta);
        assertEquals(1, neper.convert(0), delta);
        assertEquals(Math.E, neper.convert(1), delta);
        assertEquals(1.0/Math.E, neper.convert(-1), delta);
    }

    @Test
    public void inverse() {
        assertEquals(-0.1, bel.inverse(0.7943282347242815), delta);
        assertEquals(1, bel.inverse(10), delta);
        assertEquals(0, bel.inverse(1), delta);
        assertEquals(0, neper.inverse(1), delta);
        assertEquals(1, neper.inverse(Math.E), delta);
        assertEquals(-1, neper.inverse(1.0/Math.E), delta);
    }
}