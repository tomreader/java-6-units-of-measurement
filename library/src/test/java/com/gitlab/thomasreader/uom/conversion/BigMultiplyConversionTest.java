/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.junit.Assert.*;

public class BigMultiplyConversionTest {

    static BigMultiplyConversion twelve;
    static BigMultiplyConversion fiveHundred;
    static BigMultiplyConversion fourThousand;
    static double delta = 0.000001;

    @Before
    public void setUp() throws Exception {
        twelve = new BigMultiplyConversion(BigDecimal.valueOf(12));
        fiveHundred = new BigMultiplyConversion(BigDecimal.valueOf(500));
        fourThousand = new BigMultiplyConversion(BigDecimal.valueOf(4000));
    }

    @After
    public void tearDown() throws Exception {
        twelve = null;
        fiveHundred = null;
        fourThousand = null;
    }

    @Test
    public void getCoefficient() {
        assertEquals(12.0, twelve.getCoefficient(), delta);
        assertEquals(500.0, fiveHundred.getCoefficient(), delta);
        assertEquals(4000.0, fourThousand.getCoefficient(), delta);
    }

    @Test
    public void getRationalCoefficient() {
        assertEquals(BigDecRational.valueOf(BigDecimal.valueOf(12)),
                twelve.getRationalCoefficient());

        assertEquals(BigDecRational.valueOf(BigDecimal.valueOf(500)),
                fiveHundred.getRationalCoefficient());

        assertEquals(BigDecRational.valueOf(BigDecimal.valueOf(4000)),
                fourThousand.getRationalCoefficient());
    }

    @Test
    public void convert() {
        assertEquals(1.0, twelve.convert(1.0/12.0), delta);
    }

    @Test
    public void inverse() {
        assertEquals(1.0, twelve.inverse(12.0), delta);
    }

    @Test
    public void convertBigDecimal() {
        assertEquals(BigDecimal.ONE,
                twelve.convert(BigDecimal.ONE.divide(BigDecimal.valueOf(12), MathContext.DECIMAL128), MathContext.DECIMAL128)
                .stripTrailingZeros()
        );
    }

    @Test
    public void inverseBigDecimal() {
        assertEquals(BigDecimal.ONE, twelve.inverse(BigDecimal.valueOf(12), MathContext.DECIMAL128));
    }
}