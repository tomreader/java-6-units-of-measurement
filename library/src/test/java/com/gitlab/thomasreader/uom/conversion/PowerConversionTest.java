/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.junit.Assert.*;

public class PowerConversionTest {

    public PowerConversion centi;
    public PowerConversion giga;
    static double delta = 0.000001;

    @Before
    public void setUp() throws Exception {
        centi = new PowerConversion(10, -2);
        giga = new PowerConversion(10, 9);
    }

    @After
    public void tearDown() throws Exception {
        centi = null;
        giga = null;
    }

    @Test
    public void getCoefficient() {
        assertEquals(0.01, centi.getCoefficient(), delta);
        assertEquals(1000000000, giga.getCoefficient(), delta);
    }

    @Test
    public void getRationalCoefficient() {
        assertEquals(BigDecRational.valueOf(1, 100), centi.getRationalCoefficient());
        assertEquals(BigDecRational.valueOf(1000000000), giga.getRationalCoefficient());
    }

    @Test
    public void convert() {
        assertEquals(0.5, centi.convert(50), delta);
        assertEquals(500, giga.convert(0.0000005), delta);
    }

    @Test
    public void inverse() {
        assertEquals(50, centi.inverse(0.5), delta);
        assertEquals(0.0000005, giga.inverse(500), delta);
    }

    @Test
    public void convertBigDecimal() {
        assertEquals(BigDecimal.ONE, centi.convert(BigDecimal.valueOf(100), MathContext.DECIMAL128).stripTrailingZeros());
        assertEquals(BigDecimal.ONE, giga.convert(new BigDecimal("0.000000001"), MathContext.DECIMAL128).stripTrailingZeros());
    }

    @Test
    public void inverseBigDecimal() {
        assertEquals(BigDecimal.ONE, centi.inverse(new BigDecimal("0.01"), MathContext.DECIMAL128));
        assertEquals(BigDecimal.ONE, giga.inverse(new BigDecimal("1000000000"), MathContext.DECIMAL128));
    }
}