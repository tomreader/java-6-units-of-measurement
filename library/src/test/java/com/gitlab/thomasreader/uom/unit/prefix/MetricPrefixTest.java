/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit.prefix;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.unit.PrefixedUnit;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.UnitBuilder;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;

import static org.junit.Assert.*;

public class MetricPrefixTest {

    @Test
    public void prefixOrThrow() {
        Unit<Length> metre = UnitBuilder.symbol("m").referenceUnit(Length.class);
        final Unit<Length> inch =
                UnitBuilder.symbol("in").canPrefix(false).unit(metre, Conversions.multiply(0.0254));

        assertEquals(
                new PrefixedUnit<Length>(MetricPrefix.KILO, metre),
                MetricPrefix.KILO.prefixOrThrow(metre)
        );

        assertThrows(
                IllegalArgumentException.class,
                new ThrowingRunnable() {
                    @Override
                    public void run() throws Throwable {
                        MetricPrefix.KILO.prefixOrThrow(inch);
                    }
                }
        );
    }
}