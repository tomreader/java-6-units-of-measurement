/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

public class OffsetConversionTest {

//    static OffsetConversion CELCIUS_TO_KELVIN = new OffsetConversion(273.15);
//    static OffsetConversion NEGATIVE_FIFTY = new OffsetConversion(-50);
//
//    @Test
//    public void invert() {
//        assertEquals(new OffsetConversion(-273.15), CELCIUS_TO_KELVIN.invert());
//        assertEquals(new OffsetConversion(50), NEGATIVE_FIFTY.invert());
//    }
//
//    @Test
//    public void pow() {
//        assertEquals(new OffsetConversion(74610.9225), CELCIUS_TO_KELVIN.pow(2));
//        assertEquals(new OffsetConversion(2500), NEGATIVE_FIFTY.pow(2));
//    }
//
//    @Test
//    public void testEquals() {
//        assertEquals(new OffsetConversion(273.15), CELCIUS_TO_KELVIN);
//        assertEquals(new OffsetConversion(-50), NEGATIVE_FIFTY);
//        assertNotEquals(CELCIUS_TO_KELVIN, NEGATIVE_FIFTY);
//    }
//
//    @Test
//    public void testHashCode() {
//        assertEquals(new OffsetConversion(273.15).hashCode(), CELCIUS_TO_KELVIN.hashCode());
//        assertEquals(new OffsetConversion(-50).hashCode(), NEGATIVE_FIFTY.hashCode());
//        assertNotEquals(NEGATIVE_FIFTY.hashCode(), CELCIUS_TO_KELVIN.hashCode());
//    }
}