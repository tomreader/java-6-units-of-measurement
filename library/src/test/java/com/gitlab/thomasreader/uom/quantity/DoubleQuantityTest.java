/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.math.FunctionXY;
import com.gitlab.thomasreader.uom.quantity.range.QuantityRange;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.quantity.type.Temperature;
import com.gitlab.thomasreader.uom.unit.ReferenceUnit;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.UnitBuilder;
import com.gitlab.thomasreader.uom.unit.prefix.MetricPrefix;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class DoubleQuantityTest {

    static Unit<Length> METRE;
    static Unit<Length> INCH;
    static Unit<Length> MILE;

    static DoubleQuantity<Length> MILE_1;
    static DoubleQuantity<Length> MILE_2;

    static Unit<Temperature> KELVIN;
    static Unit<Temperature> CELSIUS;
    static Unit<Temperature> FAHRENHEIT;

    @Before
    public void setUp() throws Exception {
        METRE = UnitBuilder
                .symbol("m")
                .referenceUnit(Length.class);
        INCH = UnitBuilder.symbol("in")
                .canPrefix(false)
                .unit(METRE, Conversions.multiply(0.0254));
        MILE = UnitBuilder.symbol("mi")
                .canPrefix(false)
                .unit(METRE, Conversions.multiply(1609.344));

        MILE_1 = new DoubleQuantity<Length>(1, MILE);
        MILE_2 = new DoubleQuantity<Length>(2, MILE);

        KELVIN = UnitBuilder
                .symbol("K")
                .referenceUnit(Temperature.class);
        CELSIUS = UnitBuilder
                .symbol("°C")
                .unit(KELVIN, Conversions.offset(273.15));
        FAHRENHEIT = UnitBuilder
                .symbol("°F")
                .canPrefix(false)
                .unit(KELVIN, Conversions.offsetThenCoefficient(
                        Conversions.offset(459.67),
                        Conversions.multiply(5.0 / 9.0)
                ));
    }

    @After
    public void tearDown() throws Exception {
        METRE = null;
        INCH = null;
        MILE = null;
        MILE_1 = null;
        MILE_2 = null;
        KELVIN = null;
        CELSIUS = null;
        FAHRENHEIT = null;
    }

    @Test
    public void min() {
        assertEquals(MILE_1, MILE_1.min(MILE_2));
        assertEquals(MILE_1, MILE_2.min(MILE_1));
    }

    @Test
    public void max() {
        assertEquals(MILE_2, MILE_1.max(MILE_2));
        assertEquals(MILE_2, MILE_2.max(MILE_1));
    }

    @Test
    public void plusOrMinus() {
        QuantityRange<Length> range = MILE_2.plusOrMinus(MILE_1);
        assertEquals(0, range.getStart().compareTo(MILE_1));
        assertEquals(
                0,
                range.getEnd().compareTo(new DoubleQuantity<Length>(3, MILE)));
    }

    @Test
    public void to() {
        assertEquals(
                1 * 1609.344 / 0.0254,
                MILE_1.to(INCH).doubleValue(),
                0.00000001
        );
    }

    @Test
    public void compareTo() {
        assertEquals(0, MILE_1.compareTo(MILE_1));
        assertEquals(-1, MILE_1.compareTo(MILE_2));
        assertEquals(1, MILE_2.compareTo(MILE_1));
    }

    @Test
    public void compareToEpsilon() {
        // 1 / 2 = 0.5 so epsilon should consider them equal
        double epsilon = 0.5;
        assertEquals(0, MILE_1.compareTo(MILE_1, epsilon));
        assertEquals(0, MILE_1.compareTo(MILE_2, epsilon));
        assertEquals(0, MILE_2.compareTo(MILE_1, epsilon));
    }

    @Test
    public void add() {
        assertEquals(0, MILE_1.add(MILE_1).compareTo(MILE_2));

        assertEquals(
                0,
                new DoubleQuantity<Temperature>(0, CELSIUS).add(
                        new DoubleQuantity<Temperature>(10, CELSIUS, true)
                ).compareTo(new DoubleQuantity<Temperature>(10, CELSIUS), 0.00001)
        );
    }

    @Test
    public void subtract() {
        assertEquals(0, MILE_2.subtract(MILE_1).compareTo(MILE_1));

        assertEquals(
                0,
                new DoubleQuantity<Temperature>(10, CELSIUS).subtract(
                        new DoubleQuantity<Temperature>(10, CELSIUS, true)
                ).compareTo(new DoubleQuantity<Temperature>(0, CELSIUS), 0.00001)
        );
    }

    @Test
    public void interval() {
                assertEquals(
                0,
                MILE_2.interval(MILE_1)
                        .compareTo(new DoubleQuantity<Length>(1, MILE, true))
        );
    }

    @Test
    public void multiply() {
        assertEquals(MILE_2.doubleValue(), MILE_1.multiply(2).doubleValue(), 0.000001);
    }

    @Test
    public void multiplyBigDecimal() {
        assertEquals(0, MILE_1.multiply(BigDecimal.valueOf(2)).compareTo(MILE_2));
    }

    @Test
    public void divide() {
        assertEquals(MILE_1.doubleValue(), MILE_2.divide(2).doubleValue(), 0.000001);
    }

    @Test
    public void divideBigDecimal() {
        assertEquals(0, MILE_2.divide(BigDecimal.valueOf(2)).compareTo(MILE_1));
    }

    @Test
    public void divideQuantityT() {
        assertEquals(
                0,
                MILE_2
                        .divide(MILE_2)
                        .compareTo(new DoubleQuantity<Dimensionless>(1, ReferenceUnit.ONE))
        );
    }

    @Test
    public void negate() {
        assertEquals(
                0,
                MILE_1.negate()
                        .compareTo(new DoubleQuantity<Length>(-1, MILE))
        );
    }

    @Test
    public void inc() {
        assertEquals(0, MILE_1.inc().compareTo(MILE_2));
    }

    @Test
    public void dec() {
        assertEquals(0, MILE_2.dec().compareTo(MILE_1));
    }

    @Test
    public void rint() {
        assertEquals(
                12,
                new DoubleQuantity<Length>(11.99999996, METRE)
                    .rint(0.00000005)
                    .doubleValue(),
                0
        );
    }

    @Test
    public void relativeDifference() {
        double epsilon = 0.00000001;
        assertEquals(
                0,
                MILE_1.relativeDifference(MILE_1, FunctionXY.MAX_OF_ABSOLUTES).doubleValue(),
                epsilon
        );

        assertEquals(
                0.5,
                MILE_2.relativeDifference(MILE_1, FunctionXY.MAX_OF_ABSOLUTES).doubleValue(),
                epsilon
        );

        assertEquals(
                0.5,
                MILE_1.relativeDifference(MILE_2, FunctionXY.MAX_OF_ABSOLUTES).doubleValue(),
                epsilon
        );

        assertEquals(
                1,
                MILE_2.relativeDifference(MILE_1, FunctionXY.MIN_OF_ABSOLUTES).doubleValue(),
                epsilon
        );

        assertEquals(
                1,
                MILE_1.relativeDifference(MILE_2, FunctionXY.MIN_OF_ABSOLUTES).doubleValue(),
                epsilon
        );
    }

    @Test
    public void relativeChange() {
        double epsilon = 0.00000001;
        assertEquals(0, MILE_1.relativeChange(MILE_1).doubleValue(), epsilon);
        assertEquals(1, MILE_2.relativeChange(MILE_1).doubleValue(), epsilon);
        assertEquals(-0.5, MILE_1.relativeChange(MILE_2).doubleValue(), epsilon);
    }


    @Test
    public void toHuman() {
        assertEquals(MILE_1, MILE_1.toHuman());

        DoubleQuantity<Length> human1 = new DoubleQuantity<Length>(1E30, METRE).toHuman();
        DoubleQuantity<Length> result1 = new DoubleQuantity<Length>(
                1E6,
                MetricPrefix.YOTTA.prefixOrThrow(METRE)
        );

        assertEquals(
                result1.doubleValue(),
                human1.doubleValue(),
                0.000000001
        );

        assertEquals(
                result1.getUnit(),
                human1.getUnit()
        );

        DoubleQuantity<Length> human2 = new DoubleQuantity<Length>(1E-30, METRE).toHuman();
        DoubleQuantity<Length> result2 = new DoubleQuantity<Length>(
                1E-6,
                MetricPrefix.YOCTO.prefixOrThrow(METRE)
        );

        assertEquals(
                result2.doubleValue(),
                human2.doubleValue(),
                0.000000001
        );

        assertEquals(
                result2.getUnit(),
                human2.getUnit()
        );
    }
}