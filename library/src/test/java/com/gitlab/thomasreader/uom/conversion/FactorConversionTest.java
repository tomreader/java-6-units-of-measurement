/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

public class FactorConversionTest {

//    static MultiplicationFactor INCH_TO_METRE_CONVERSION = new MultiplicationFactor(0.0254);
//    static FactorConversion LIGHT_SECOND_TO_METRE_CONVERSION = new MultiplicationFactor(299792458);
//    static MathContext MATH_CONTEXT = Conversion.DEFAULT_MATH_CONTEXT;
//
//    @Test
//    public void invert() {
//        assertEquals(
//                new MultiplicationFactor(
//                        BigDecimal.ONE.divide(BigDecimal.valueOf(0.0254), MATH_CONTEXT)
//                        .doubleValue()
//                ),
//                INCH_TO_METRE_CONVERSION.invert()
//        );
//
//        assertEquals(
//                new MultiplicationFactor(
//                        BigDecimal.ONE.divide(BigDecimal.valueOf(299792458), MATH_CONTEXT)
//                                .doubleValue()
//                ),
//                LIGHT_SECOND_TO_METRE_CONVERSION.invert()
//        );
//
//        assertNotEquals(INCH_TO_METRE_CONVERSION.invert(), LIGHT_SECOND_TO_METRE_CONVERSION.invert());
//
//        assertEquals(INCH_TO_METRE_CONVERSION.getFactor(),
//                INCH_TO_METRE_CONVERSION.invert().invert().getFactor(),
//                (INCH_TO_METRE_CONVERSION.getFactor() / Math.pow(10, 6)));
//    }
//
//    @Test
//    public void pow() {
//        assertEquals(new MultiplicationFactor(0.00064516), INCH_TO_METRE_CONVERSION.pow(2));
//        assertEquals(new MultiplicationFactor(89875517873681764d), LIGHT_SECOND_TO_METRE_CONVERSION.pow(2));
//    }
//
//    @Test
//    public void testEquals() {
//        assertEquals(new MultiplicationFactor(0.0254), INCH_TO_METRE_CONVERSION);
//        assertEquals(new MultiplicationFactor(299792458), LIGHT_SECOND_TO_METRE_CONVERSION);
//        assertNotEquals(INCH_TO_METRE_CONVERSION, LIGHT_SECOND_TO_METRE_CONVERSION);
//    }
//
//    @Test
//    public void testHashCode() {
//        assertEquals(new MultiplicationFactor(0.0254).hashCode(), INCH_TO_METRE_CONVERSION.hashCode());
//        assertEquals(new MultiplicationFactor(299792458).hashCode(), LIGHT_SECOND_TO_METRE_CONVERSION.hashCode());
//        assertNotEquals(INCH_TO_METRE_CONVERSION.hashCode(), LIGHT_SECOND_TO_METRE_CONVERSION.hashCode());
//    }
}