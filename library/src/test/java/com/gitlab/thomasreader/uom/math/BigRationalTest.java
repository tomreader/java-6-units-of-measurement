/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.math;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Random;

import static org.junit.Assert.*;


public class BigRationalTest {

    // Inch -> Metre conversion (* 0.0254)
    private static BigRational frac254Div10000;
    // Foot -> Metre conversion (* 0.3048)
    private static BigRational frac3048Div10000;
    // Mile -> Metre conversion (* 1609.344)
    private static BigRational frac1609344Div1000;

    @BeforeClass
    public static void setUp() {
        frac254Div10000 = new BigRational(BigInteger.valueOf(254), BigInteger.valueOf(10000));
        frac3048Div10000 = new BigRational(BigInteger.valueOf(3048), BigInteger.valueOf(10000));
        frac1609344Div1000 = new BigRational(BigInteger.valueOf(1609344), BigInteger.valueOf(1000));
    }

    @AfterClass
    public static void tearDown() {
        frac254Div10000 = null;
        frac3048Div10000 = null;
        frac1609344Div1000 = null;
    }

    @Test
    public void valueOfBigIntegerBigInteger() {
        final BigInteger a = BigInteger.valueOf(300);
        BigInteger b = BigInteger.valueOf(250);
        BigRational aDivB = BigRational.valueOf(a, b);
        assertEquals(a, aDivB.getNumerator());
        assertEquals(b, aDivB.getDenominator());

        BigInteger c = BigInteger.valueOf(300);
        BigRational aDivC = BigRational.valueOf(a, c);
        assertEquals(BigInteger.ONE, aDivC.getNumerator());
        assertEquals(BigInteger.ONE, aDivC.getDenominator());

        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigRational.valueOf(a, BigInteger.ZERO);
            }
        });
    }

    @Test
    public void valueOfLongLong() {
        long a = 300L;
        long b = 250L;
        BigRational aDivB = BigRational.valueOf(a, b);
        assertEquals(a, aDivB.getNumerator().longValue());
        assertEquals(b, aDivB.getDenominator().longValue());

        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigRational.valueOf(300L, 0L);
            }
        });
    }

    @Test
    public void valueOfBigInteger() {
        BigInteger a = BigInteger.valueOf(300);
        BigInteger one = new BigInteger("1");

        BigRational rationalA = BigRational.valueOf(a);
        BigRational rationalOne = BigRational.valueOf(one);

        assertEquals(a, rationalA.getNumerator());
        assertEquals(BigInteger.ONE, rationalA.getDenominator());
        assertEquals(a, rationalA.bigIntegerValue());

        assertEquals(BigRational.ONE, rationalOne);
        assertEquals(BigInteger.ONE, rationalOne.bigIntegerValue());
    }

    @Test
    public void valueOfLong() {
        long a = 300L;
        long one = 1L;

        BigRational rationalA = BigRational.valueOf(a);
        BigRational rationalOne = BigRational.valueOf(one);

        assertEquals(a, rationalA.getNumerator().longValue());
        assertEquals(BigInteger.ONE, rationalA.getDenominator());
        assertEquals(a, rationalA.longValue());

        assertEquals(BigRational.ONE, rationalOne);
        assertEquals(one, rationalOne.longValue());
    }

    @Test
    public void valueOfBigDecimal() {
        BigDecimal inch = new BigDecimal("0.0254");
        BigDecimal mile = new BigDecimal("1609.344");

        BigRational rationalInch = BigRational.valueOf(inch);
        BigRational rationalMile = BigRational.valueOf(mile);

        assertEquals(frac254Div10000, rationalInch);
        assertEquals(frac1609344Div1000, rationalMile);
    }

    @Test
    public void valueOfDouble() {
        double pi = Math.PI;
        double e = Math.E;
        double deltaDividend = Math.pow(10, 6);

        BigRational rationalPi = BigRational.valueOf(pi);
        BigRational rationalE = BigRational.valueOf(e);

        assertEquals(pi, rationalPi.doubleValue(), pi / deltaDividend);
        assertEquals(e, rationalE.doubleValue(), e / deltaDividend);
    }

    @Test
    public void bigIntegerValue() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigRational rationalPi = BigRational.valueOf(pi);

        assertEquals(BigInteger.valueOf(3), rationalPi.bigIntegerValue());
    }

    @Test
    public void bigDecimalValue() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigRational rationalPi = BigRational.valueOf(pi);

        assertEquals(new BigDecimal("3.1415926535"), rationalPi.bigDecimalValue());
    }

    @Test
    public void bigDecimalValueMathContext() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigRational rationalPi = BigRational.valueOf(pi);
        MathContext mathContext = MathContext.DECIMAL32;

        assertEquals(new BigDecimal("3.1415926535").round(mathContext), rationalPi.bigDecimalValue(mathContext));
    }

    @Test
    public void simplify() {
        BigInteger oneHundred = BigInteger.valueOf(100);
        BigInteger tenThousand = BigInteger.valueOf(10000);
        BigInteger gcd1 = oneHundred.gcd(tenThousand);

        BigRational bigRational = new BigRational(oneHundred, tenThousand);
        BigRational simpBigRational = bigRational.simplify();
        assertEquals(oneHundred.divide(gcd1), simpBigRational.getNumerator());
        assertEquals(tenThousand.divide(gcd1), simpBigRational.getDenominator());
    }

    @Test
    public void signum() {
        BigRational minusOne = BigRational.valueOf(-1);
        BigRational minusOneDivMinusThree = new BigRational(BigInteger.valueOf(-1), BigInteger.valueOf(-3));

        assertEquals(0, BigRational.ZERO.signum());
        assertEquals(1, BigRational.ONE.signum());
        assertEquals(-1, minusOne.signum());
        assertEquals(1, minusOneDivMinusThree.signum());
    }

    @Test
    public void negate() {
        assertEquals(-1, BigRational.ONE.negate().signum());
        assertEquals(0, BigRational.ZERO.negate().signum());
        assertEquals(1, BigRational.valueOf(-1).negate().signum());
    }

    @Test
    public void isZero() {
        assertTrue(BigRational.ZERO.isZero());
        assertFalse(BigRational.ONE.isZero());
    }

    @Test
    public void reciprocal() {
        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigRational.ZERO.reciprocal();
            }
        });

        assertEquals(BigRational.ONE, BigRational.ONE.reciprocal());

        assertEquals(
                BigRational.valueOf(BigInteger.valueOf(10000), BigInteger.valueOf(254)),
                frac254Div10000.reciprocal());
    }

    @Test
    public void isInteger() {
        assertTrue(BigRational.ONE.isInteger());
        assertTrue(BigRational.ZERO.isInteger());
        assertFalse(frac254Div10000.isInteger());
        assertFalse(frac3048Div10000.isInteger());
    }

    @Test
    public void isProper() {
        assertTrue(frac254Div10000.isProper());
        assertTrue(frac3048Div10000.isProper());
        assertFalse(BigRational.ONE.isProper());
    }

    @Test
    public void getProperFractionPart() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigRational rationalPi = BigRational.valueOf(pi);
        BigRational rationalPiMinusThree = BigRational.valueOf(new BigDecimal("0.1415926535"));

        assertEquals(rationalPiMinusThree, rationalPi.getProperFractionPart());
    }

    @Test
    public void getIntegerPart() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigRational rationalPi = BigRational.valueOf(pi);
        BigInteger three = BigInteger.valueOf(3);

        assertEquals(three, rationalPi.getIntegerPart());
    }

    @Test
    public void abs() {
        BigRational minusThree = BigRational.valueOf(-3);

        assertEquals(1, minusThree.abs().signum());
        assertEquals(-1, minusThree.signum());
    }

    @Test
    public void inc() {
        BigRational two = BigRational.valueOf(2);
        BigRational three = BigRational.valueOf(3);
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational piPlusOne = BigRational.valueOf(new BigDecimal("4.1415926535"));

        assertEquals(three, two.inc());
        assertEquals(piPlusOne, pi.inc());
    }

    @Test
    public void dec() {
        BigRational two = BigRational.valueOf(2);
        BigRational three = BigRational.valueOf(3);
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational piPlusOne = BigRational.valueOf(new BigDecimal("4.1415926535"));

        assertEquals(two, three.dec());
        assertEquals(pi, piPlusOne.dec());
    }

    @Test
    public void add() {
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational negativePi = BigRational.valueOf(new BigDecimal("-3.1415926535"));
        BigRational tau = BigRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(tau.bigDecimalValue(), pi.add(pi).bigDecimalValue());
        assertEquals(pi.bigDecimalValue(), tau.add(negativePi).bigDecimalValue());
        assertEquals(BigRational.ZERO.bigDecimalValue(), pi.add(negativePi).bigDecimalValue());
    }

    @Test
    public void subtract() {
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational negativePi = BigRational.valueOf(new BigDecimal("-3.1415926535"));
        BigRational tau = BigRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(BigRational.ZERO.bigDecimalValue(), pi.subtract(pi).bigDecimalValue());
        assertEquals(pi.bigDecimalValue(), tau.subtract(pi).bigDecimalValue());
        assertEquals(tau.bigDecimalValue(), pi.subtract(negativePi).bigDecimalValue());
    }

    @Test
    public void multiply() {
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational tau = BigRational.valueOf(new BigDecimal("6.283185307"));
        BigRational two = BigRational.valueOf(2);
        BigRational half = BigRational.valueOf(BigInteger.ONE, BigInteger.valueOf(2));

        assertEquals(tau.bigDecimalValue(), pi.multiply(two).bigDecimalValue());
        assertEquals(pi.bigDecimalValue(), tau.multiply(half).bigDecimalValue());
    }

    @Test
    public void divide() {
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational tau = BigRational.valueOf(new BigDecimal("6.283185307"));
        BigRational two = BigRational.valueOf(2);
        BigRational half = BigRational.valueOf(BigInteger.ONE, BigInteger.valueOf(2));

        assertEquals(pi.bigDecimalValue(), tau.divide(two).bigDecimalValue());
        assertEquals(tau.bigDecimalValue(), pi.divide(half).bigDecimalValue());
    }

    @Test
    public void pow() {
        BigInteger ten = BigInteger.TEN;
        BigInteger tenThousand = BigInteger.valueOf(10000);
        BigRational tenDivTenThousand = BigRational.valueOf(ten, tenThousand);
        int exponent = 4;
        BigRational result = BigRational.valueOf(
                ten.pow(exponent),
                tenThousand.pow(exponent)
        );

        assertEquals(result, tenDivTenThousand.pow(exponent));

        int negExponent = -4;
        BigRational negResult = BigRational.valueOf(
                tenThousand.pow(Math.abs(negExponent)),
                ten.pow(Math.abs(negExponent))
        );

        assertEquals(negResult, tenDivTenThousand.pow(negExponent));
    }

    @Test
    public void max() {
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational tau = BigRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(tau, pi.max(tau));
        assertEquals(tau, tau.max(pi));
    }

    @Test
    public void maxArray() {
        Random random = new Random();
        BigRational[] rationals = new BigRational[10];
        long startingLong = random.nextLong();
        BigRational startingRational = BigRational.valueOf(startingLong);
        long largestRational = startingLong;

        for (int i = 0; i < rationals.length; i++) {
            long nextLong = random.nextLong();
            if (nextLong > largestRational) {
                largestRational = nextLong;
            }
            rationals[i] = BigRational.valueOf(nextLong);
        }
        assertEquals(BigRational.valueOf(largestRational), startingRational.max(rationals));
    }

    @Test
    public void min() {
        BigRational pi = BigRational.valueOf(new BigDecimal("3.1415926535"));
        BigRational tau = BigRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(pi, pi.min(tau));
        assertEquals(pi, tau.min(pi));
    }

    @Test
    public void minArray() {
        Random random = new Random();
        BigRational[] rationals = new BigRational[10];
        long startingLong = random.nextLong();
        BigRational startingRational = BigRational.valueOf(startingLong);
        long smallestRational = startingLong;

        for (int i = 0; i < rationals.length; i++) {
            long nextLong = random.nextLong();
            if (nextLong < smallestRational) {
                smallestRational = nextLong;
            }
            rationals[i] = BigRational.valueOf(nextLong);
        }

        assertEquals(BigRational.valueOf(smallestRational), startingRational.min(rationals));
    }

    @Test
    public void compareTo() {
        assertEquals(0, new BigRational(BigInteger.ONE, BigInteger.ONE).compareTo(BigRational.ONE));
        assertEquals(-1, BigRational.ZERO.compareTo(BigRational.ONE));
        assertEquals(1, BigRational.ONE.compareTo(BigRational.ZERO));
    }

    @Test
    public void testToString() {
        assertEquals("1", BigRational.ONE.toString());
        assertEquals("0", BigRational.ZERO.toString());
        assertEquals("1/3", BigRational.valueOf(BigInteger.ONE, BigInteger.valueOf(3)).toString());
    }

    @Test
    public void testEquals() {
        assertEquals(frac254Div10000, frac254Div10000);
        assertNotEquals(frac254Div10000, frac3048Div10000);
    }
}