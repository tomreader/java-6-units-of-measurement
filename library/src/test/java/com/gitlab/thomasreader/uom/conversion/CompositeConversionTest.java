/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.junit.Assert.*;

public class CompositeConversionTest {

    static CompositeConversion reaumur;
    static CompositeConversion fahrenheit;
    static double delta = 0.000001;

    @Before
    public void setUp() throws Exception {
        reaumur = new CompositeConversion(Conversions.multiply(1.25), Conversions.offset(273.15));
        fahrenheit = new CompositeConversion(Conversions.offset(459.67), Conversions.multiply(5.0/9.0));
    }

    @After
    public void tearDown() throws Exception {
        reaumur = null;
        fahrenheit = null;
    }

    @Test
    public void convert() {
        assertEquals(0.0, reaumur.convert(-218.52), delta);
        assertEquals(153.15, reaumur.convert(-96.0), delta);
        assertEquals(0.0, fahrenheit.convert(-459.67), delta);
        assertEquals(593.15, fahrenheit.convert(608.0), delta);
    }

    @Test
    public void inverse() {
        assertEquals(-218.52, reaumur.inverse(0.0), delta);
        assertEquals(-96.0, reaumur.inverse(153.15), delta);
        assertEquals(-459.67, fahrenheit.inverse(0.0), delta);
        assertEquals(608.0, fahrenheit.inverse(593.15), delta);
    }

    @Test
    public void convertBigDecimal() {
        assertEquals(0, reaumur.convert(new BigDecimal("-218.52"), MathContext.DECIMAL128).doubleValue(), delta);
        assertEquals(153.15, reaumur.convert(new BigDecimal("-96.0"), MathContext.DECIMAL128).doubleValue(), delta);
        assertEquals(0, fahrenheit.convert(new BigDecimal("-459.67"), MathContext.DECIMAL128).doubleValue(), delta);
        assertEquals(593.15, fahrenheit.convert(new BigDecimal("608.0"), MathContext.DECIMAL128).doubleValue(), delta);
    }

    @Test
    public void inverseBigDecimal() {
        assertEquals(new BigDecimal("-218.52").doubleValue(), reaumur.inverse(BigDecimal.ZERO, MathContext.DECIMAL128).doubleValue(), delta);
        assertEquals(new BigDecimal("-96").doubleValue(), reaumur.inverse(new BigDecimal("153.15"), MathContext.DECIMAL128).doubleValue(), delta);
        assertEquals(new BigDecimal("-459.67").doubleValue(), fahrenheit.inverse(BigDecimal.ZERO, MathContext.DECIMAL128).doubleValue(), delta);
        assertEquals(new BigDecimal("608.0").doubleValue(), fahrenheit.inverse(new BigDecimal("593.15"), MathContext.DECIMAL128).doubleValue(), delta);
    }
}