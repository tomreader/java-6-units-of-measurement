/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.LogExpConversion;
import com.gitlab.thomasreader.uom.quantity.DoubleQuantity;
import com.gitlab.thomasreader.uom.unit.prefix.MetricPrefix;
import com.gitlab.thomasreader.uom.util.SuperscriptFormat;
import com.gitlab.thomasreader.uom.quantity.type.Area;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.quantity.type.Power;
import com.gitlab.thomasreader.uom.quantity.type.Temperature;
import com.gitlab.thomasreader.uom.quantity.type.Volume;
import com.gitlab.thomasreader.uom.unit.AliasedUnit;
import com.gitlab.thomasreader.uom.unit.DerivedUnit;
import com.gitlab.thomasreader.uom.unit.ReferenceUnit;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.UnitBuilder;

public class Sample {

    static final Unit<Length> METRE = UnitBuilder.symbol("m").referenceUnit(Length.class);
    static final Unit<Length> INCH = UnitBuilder
            .symbol("in")
            .canPrefix(false)
            .unit(METRE, Conversions.multiply(0.0254));

    static final Unit<Temperature> KELVIN = UnitBuilder.symbol("K").referenceUnit(Temperature.class);
    static final Unit<Temperature> CELSIUS = UnitBuilder
            .symbol("°C")
            .unit(KELVIN, Conversions.offset(273.15));

    static final Unit<Dimensionless> UNO = UnitBuilder.symbol("U").alias(ReferenceUnit.ONE);
    static final Unit<Dimensionless> BEL = UnitBuilder
            .symbol("B")
            .unit(ReferenceUnit.ONE, LogExpConversion.exp(10));
    static final Unit<Dimensionless> DECIBEL = MetricPrefix.DECI.prefixOrThrow(BEL);

    static final Unit<Power> WATT = UnitBuilder.symbol("W").referenceUnit(Power.class);

    public static void main(String[] args) {

        System.out.println(METRE.of(50).to(INCH));

        System.out.print("Should be ~5.3 B -> ");
        System.out.println(BEL.of(5).add(BEL.of(5)));
        System.out.print("Should be ~8 dB -> ");
        System.out.println(DECIBEL.of(5).multiply(2));

        System.out.println("Calculate dB ratio between 1000 MW and 500 KW: should be ~33 dB -> ");
        System.out.println(
                MetricPrefix.MEGA.prefixOrThrow(WATT).of(1000)
                        .divide(MetricPrefix.KILO.prefixOrThrow(WATT).of(500))
                .to(DECIBEL)
        );

        System.out.print("Should be 30 °C -> ");
        System.out.println(CELSIUS.of(0).add(new DoubleQuantity<Temperature>(30, CELSIUS, true)));
        System.out.println(CELSIUS.of(0).to(KELVIN).add(new DoubleQuantity<Temperature>(30, CELSIUS, true)).to(KELVIN).to(CELSIUS));


        Unit<Area> sqMetre = UnitBuilder
                .symbol("m" + SuperscriptFormat.fromInt(2))
                .referenceUnit(Area.class);

        DerivedUnit<Area> derivedSqMetre = DerivedUnit.builder().multiply(METRE, 2).buildDouble(sqMetre);
        DerivedUnit<Area> sqIn = DerivedUnit.builder().multiply(INCH, 2).buildDouble(sqMetre);

        System.out.print("Should be 30 m" + SuperscriptFormat.fromInt(2) + " -> ");
        System.out.println(sqMetre.of(30).to(derivedSqMetre));

        System.out.print("Should be ~1550 in" + SuperscriptFormat.fromInt(2) + " -> ");
        System.out.println(derivedSqMetre.of(1).to(sqIn));


        Unit<Volume> cuMetre = new ReferenceUnit<Volume>("m" + SuperscriptFormat.fromInt(3), Volume.class);
        DerivedUnit<Volume> cuDeciMetre = DerivedUnit.builder().multiply(MetricPrefix.DECI.prefix(METRE), 3).buildDecimal(cuMetre);
        AliasedUnit<Volume> litre = new AliasedUnit<Volume>("L", cuDeciMetre);

        System.out.println(litre.of(2).to(cuMetre));
    }

}