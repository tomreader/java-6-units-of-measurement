# Java 6 compatible Units of Measurement Library
Typed and convertible quantities using generics with support for BigDecimal and double quantities which are compatible with each other through a common interface Quantity. 
   
## Examples
### Simple conversion between 50 m -> in
``` java
Unit<Length> metre = UnitBuilder.symbol("m").referenceUnit(Length.class);
Unit<Length> inch = UnitBuilder
            .symbol("in")
            .canPrefix(false)
            .unit(METRE, Conversions.multiply(0.0254));

System.out.println(METRE.of(50).to(INCH)); 
```
prints: `1968.5039370078741 in`

### Support for temperature intervals
```java
Unit<Temperature> kelvin = UnitBuilder.symbol("K").referenceUnit(Temperature.class);
Unit<Temperature> celsius = UnitBuilder
            .symbol("°C")
            .unit(KELVIN, Conversions.offset(273.15));

Quantity<Temperature> zeroCelsius = celsius.of(0);
Quantity<Temperature> thirtyDeltaCelsius = new DoubleQuantity<Temperature>(30, celsius, true);
System.out.println(zeroCelsius.add(thirtyDeltaCelsius));
```
prints: `30 °C` which is correct when different temperature units are used:
```java
System.out.println(zeroCelsius.to(kelvin).add(thirtyDeltaCelsius.to(kelvin)).to(celsius));
```
prints: `30 °C`

### Support for logarithmic units
```java
Unit<Dimensionless> bel = UnitBuilder
            .symbol("B")
            .unit(ONE, LogExpConversion.exp(10));
Unit<Dimensionless> decibel = MetricPrefix.DECI.prefixOrThrow(BEL);

System.out.println(bel.of(5).add(bel.of(5)));
System.out.println(decibel.of(5).multiply(2));
```
prints: `5.301029995663981 B` and `8.01029995663981 dB`

## License
```
   Copyright 2022 Tom Reader

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

```
